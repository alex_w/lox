#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct NodeId(u64);

impl NodeId {
    fn raw(raw: u64) -> Self {
        Self(raw)
    }
}

#[derive(Default)]
pub struct NodeIdSource(u64);

impl NodeIdSource {
    pub fn get_id(&mut self) -> NodeId {
        let node_id = NodeId::raw(self.0);
        self.0 += 1;

        node_id
    }
}
