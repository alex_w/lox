use {
    crate::{
        expr::Expr,
        node_id::NodeId,
        parser_error::{ParserError, ParserErrors},
        stmt::{Function, Stmt},
        visitor::{
            walk_assign, walk_block, walk_class, walk_function, walk_return, walk_super, walk_var,
            walk_variable, FunctionType, Visitor,
        },
    },
    std::collections::HashMap,
};

#[derive(Default)]
pub struct Resolver {
    scopes: Vec<HashMap<String, bool>>,
    errors: ParserErrors,
    resolved: HashMap<NodeId, usize>,
    current_function_type: Option<FunctionType>,
    current_class_type: Option<ClassType>,
}

#[derive(Clone)]
enum ClassType {
    Class,
    Subclass,
}

impl Resolver {
    pub fn resolve(mut self, stmts: &[Stmt]) -> Result<HashMap<NodeId, usize>, ParserErrors> {
        self.visit_stmts(stmts);

        if self.errors.any() {
            Err(self.errors)
        } else {
            Ok(self.resolved)
        }
    }

    fn start_scope(&mut self) {
        self.scopes.push(HashMap::new());
    }

    fn end_scope(&mut self) {
        self.scopes.pop();
    }

    fn declare(&mut self, name: String) {
        if let Some(scope) = self.scopes.last_mut() {
            let old_value = scope.insert(name, false);
            if old_value.is_some() {
                self.errors.add(ParserError::new(
                    None,
                    "Already a variable with this name in this scope.",
                ));
            }
        }
    }

    fn define(&mut self, name: String) {
        if let Some(scope) = self.scopes.last_mut() {
            scope.insert(name, true);
        }
    }

    fn check_variable(&mut self, node_id: NodeId, name: &str) {
        if let Some(scope) = self.scopes.last() {
            if Some(&false) == scope.get(name) {
                self.errors.add(ParserError::new(
                    None,
                    "Can't read local variable in its initializer.",
                ));
            }
        }

        self.resolve_local(node_id, name);
    }

    fn resolve_local(&mut self, node_id: NodeId, name: &str) {
        for (i, scope) in self.scopes.iter().rev().enumerate() {
            if scope.contains_key(name) {
                self.resolved.insert(node_id, i);
                return;
            }
        }
    }
}

impl Visitor for Resolver {
    fn visit_block(&mut self, block: &[Stmt], _node_id: &NodeId) {
        self.start_scope();
        walk_block(self, block);
        self.end_scope();
    }

    fn visit_class(
        &mut self,
        name: &str,
        superclass: &Option<(String, usize)>,
        methods: &[Function],
        node_id: &NodeId,
    ) {
        let enclosing_class_type = self.current_class_type.clone();
        self.current_class_type = if superclass.is_some() {
            Some(ClassType::Subclass)
        } else {
            Some(ClassType::Class)
        };

        self.declare(String::from(name));
        self.define(String::from(name));

        match superclass {
            Some((superclass, _)) if superclass == name => self
                .errors
                .add(ParserError::new(None, "A class can't inherit from itself.")),
            Some((superclass, _)) => self.resolve_local(node_id.clone(), superclass),
            None => {}
        }

        if superclass.is_some() {
            self.start_scope();
            self.define(String::from("super"));
        }

        self.start_scope();
        self.define(String::from("this"));

        walk_class(self, name, superclass, methods, node_id);

        self.end_scope();

        if superclass.is_some() {
            self.end_scope();
        }

        self.current_class_type = enclosing_class_type;
    }

    fn visit_var(&mut self, name: &str, initializer: &Option<Expr>, node_id: &NodeId) {
        self.declare(String::from(name));
        walk_var(self, name, initializer, node_id);
        self.define(String::from(name));
    }

    fn visit_variable(&mut self, name: &str, node_id: &NodeId) {
        self.check_variable(node_id.clone(), name);
        walk_variable(self, name);
    }

    fn visit_this(&mut self, node_id: &NodeId) {
        match self.current_class_type {
            None => self.errors.add(ParserError::new(
                None,
                "Can't use 'this' outside of a class.",
            )),
            _ => self.check_variable(node_id.clone(), "this"),
        }
    }

    fn visit_super(&mut self, method: &str, node_id: &NodeId) {
        match self.current_class_type {
            Some(ClassType::Subclass) => self.resolve_local(node_id.clone(), "super"),
            Some(ClassType::Class) => self.errors.add(ParserError::new(
                None,
                "Can't use 'super' in a class with no superclass.",
            )),
            None => self.errors.add(ParserError::new(
                None,
                "Can't use 'super' outside of a class.",
            )),
        }

        walk_super(self, method, node_id);
    }

    fn visit_assign(&mut self, name: &str, value: &Expr, node_id: &NodeId) {
        walk_assign(self, name, value, node_id);
        self.resolve_local(node_id.clone(), name)
    }

    fn visit_function(
        &mut self,
        function: &Function,
        function_type: FunctionType,
        _node_id: &NodeId,
    ) {
        self.declare(function.name.clone());
        self.define(function.name.clone());

        let enclosing_function_type = self.current_function_type.clone();
        self.current_function_type = Some(function_type);

        self.start_scope();
        walk_function(self, function);
        self.end_scope();

        self.current_function_type = enclosing_function_type;
    }

    fn visit_function_param(&mut self, param: &str) {
        self.declare(String::from(param));
        self.define(String::from(param));
    }

    fn visit_return(&mut self, expr: &Option<Expr>, node_id: &NodeId) {
        match self.current_function_type {
            None => self
                .errors
                .add(ParserError::new(None, "Can't return from top-level code.")),
            Some(FunctionType::Initializer) if expr.is_some() => self.errors.add(ParserError::new(
                None,
                "Can't return a value from initializer.",
            )),
            _ => {}
        }

        walk_return(self, expr, node_id);
    }
}
