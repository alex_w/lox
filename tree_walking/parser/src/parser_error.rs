use {
    scanner::token::{Token, TokenKind},
    std::{error::Error, fmt, result},
};

#[derive(Debug, PartialEq)]
pub struct ParserError {
    token: Option<Token>,
    message: String,
}

impl ParserError {
    pub fn new(token: Option<Token>, message: impl Into<String>) -> Self {
        Self {
            token,
            message: message.into(),
        }
    }
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.token {
            Some(token) if token.kind == TokenKind::Eof => {
                write!(f, "{} at end, {}", token.line, self.message)
            }
            Some(token) => {
                write!(f, "{}, {}", token.line, self.message)
            }
            None => {
                write!(f, "{}", self.message)
            }
        }
    }
}

impl Error for ParserError {}

pub type Result<T> = result::Result<T, ParserError>;

#[derive(Debug, PartialEq, Default)]
pub struct ParserErrors {
    errors: Vec<ParserError>,
}

impl ParserErrors {
    pub fn add(&mut self, err: ParserError) {
        self.errors.push(err);
    }

    pub fn any(&self) -> bool {
        !self.errors.is_empty()
    }
}

impl fmt::Display for ParserErrors {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let last_i = self.errors.len().saturating_sub(1);
        for (i, error) in self.errors.iter().enumerate() {
            write!(f, "{}", error)?;

            if i < last_i {
                writeln!(f)?
            }
        }

        Ok(())
    }
}

impl Error for ParserErrors {}
