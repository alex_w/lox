#![allow(dead_code)]

use crate::{
    expr::{Expr, ExprKind},
    node_id::NodeId,
    stmt::{Function, Stmt, StmtKind},
    BinaryOp, LiteralKind, LogicalOp, UnaryOp,
};

pub trait Visitor: Sized {
    fn visit_stmts(&mut self, stmts: &[Stmt]) {
        walk_stmts(self, stmts);
    }

    fn visit_stmt(&mut self, stmt: &Stmt) {
        walk_stmt(self, stmt);
    }

    fn visit_block(&mut self, block: &[Stmt], _node_id: &NodeId) {
        walk_block(self, block);
    }

    fn visit_class(
        &mut self,
        name: &str,
        superclass: &Option<(String, usize)>,
        methods: &[Function],
        node_id: &NodeId,
    ) {
        walk_class(self, name, superclass, methods, node_id);
    }

    fn visit_expr(&mut self, expr: &Expr, _node_id: &NodeId) {
        walk_expr(self, expr);
    }

    fn visit_function(
        &mut self,
        function: &Function,
        _function_type: FunctionType,
        _node_id: &NodeId,
    ) {
        walk_function(self, function);
    }

    fn visit_function_param(&mut self, _param: &str) {}

    fn visit_if(
        &mut self,
        condition: &Expr,
        then_branch: &Stmt,
        else_branch: &Option<Box<Stmt>>,
        node_id: &NodeId,
    ) {
        walk_if(self, condition, then_branch, else_branch, node_id);
    }

    fn visit_print(&mut self, expr: &Expr, node_id: &NodeId) {
        walk_print(self, expr, node_id);
    }

    fn visit_return(&mut self, expr: &Option<Expr>, node_id: &NodeId) {
        walk_return(self, expr, node_id);
    }

    fn visit_var(&mut self, name: &str, initializer: &Option<Expr>, node_id: &NodeId) {
        walk_var(self, name, initializer, node_id);
    }

    fn visit_while(&mut self, condition: &Expr, body: &Stmt, node_id: &NodeId) {
        walk_while(self, condition, body, node_id);
    }

    fn visit_name(&mut self, _name: &str) {
        // Nothing to do
    }

    fn visit_assign(&mut self, name: &str, value: &Expr, node_id: &NodeId) {
        walk_assign(self, name, value, node_id);
    }

    fn visit_binary(&mut self, left: &Expr, operator: &BinaryOp, right: &Expr, node_id: &NodeId) {
        walk_binary(self, left, operator, right, node_id);
    }

    fn visit_call(&mut self, callee: &Expr, arguments: &[Expr], node_id: &NodeId) {
        walk_call(self, callee, arguments, node_id);
    }

    fn visit_get(&mut self, object: &Expr, name: &str, node_id: &NodeId) {
        walk_get(self, object, name, node_id);
    }

    fn visit_grouping(&mut self, expr: &Expr, node_id: &NodeId) {
        walk_grouping(self, expr, node_id);
    }

    fn visit_literal(&mut self, _literal: &LiteralKind, _node_id: &NodeId) {
        // Nothing to do
    }

    fn visit_logical(&mut self, left: &Expr, operator: &LogicalOp, right: &Expr, node_id: &NodeId) {
        walk_logical(self, left, operator, right, node_id);
    }

    fn visit_set(&mut self, object: &Expr, name: &str, value: &Expr, node_id: &NodeId) {
        walk_set(self, object, name, value, node_id);
    }

    fn visit_super(&mut self, method: &str, node_id: &NodeId) {
        walk_super(self, method, node_id);
    }

    fn visit_unary(&mut self, operator: &UnaryOp, right: &Expr, node_id: &NodeId) {
        walk_unary(self, operator, right, node_id);
    }

    fn visit_this(&mut self, _node_id: &NodeId) {
        // Nothing to do
    }

    fn visit_variable(&mut self, name: &str, _node_id: &NodeId) {
        walk_variable(self, name);
    }
}

#[derive(Clone)]
pub enum FunctionType {
    Function,
    Initializer,
    Method,
}

macro_rules! walk_list {
    ($visitor: expr, $method: ident, $list: expr $(, $opt: expr)*) => {
        for elem in $list {
            $visitor.$method(elem, $($opt),*)
        }
    };
}

pub fn walk_stmts<V: Visitor>(visitor: &mut V, stmts: &[Stmt]) {
    walk_list!(visitor, visit_stmt, stmts);
}

pub fn walk_stmt<V: Visitor>(visitor: &mut V, stmt: &Stmt) {
    match stmt.kind {
        StmtKind::Block(ref block) => visitor.visit_block(block, &stmt.id),
        StmtKind::Class {
            ref name,
            ref superclass,
            ref methods,
        } => visitor.visit_class(name, superclass, methods, &stmt.id),
        StmtKind::Expr(ref expr) => visitor.visit_expr(expr, &stmt.id),
        StmtKind::Function(ref function) => {
            visitor.visit_function(function, FunctionType::Function, &stmt.id)
        }
        StmtKind::If {
            ref condition,
            ref then_branch,
            ref else_branch,
        } => visitor.visit_if(condition, then_branch, else_branch, &stmt.id),
        StmtKind::Print(ref expr) => visitor.visit_print(expr, &stmt.id),
        StmtKind::Return(ref expr) => visitor.visit_return(expr, &stmt.id),
        StmtKind::Var {
            ref name,
            ref initializer,
        } => visitor.visit_var(name, initializer, &stmt.id),
        StmtKind::While {
            ref condition,
            ref body,
        } => visitor.visit_while(condition, body, &stmt.id),
    }
}

pub fn walk_block<V: Visitor>(visitor: &mut V, block: &[Stmt]) {
    walk_list!(visitor, visit_stmt, block);
}

pub fn walk_class<V: Visitor>(
    visitor: &mut V,
    name: &str,
    _superclass: &Option<(String, usize)>,
    methods: &[Function],
    node_id: &NodeId,
) {
    visitor.visit_name(name);

    for method in methods {
        let function_type = if method.name == "init" {
            FunctionType::Initializer
        } else {
            FunctionType::Method
        };

        visitor.visit_function(method, function_type, node_id);
    }
}

pub fn walk_expr<V: Visitor>(visitor: &mut V, expr: &Expr) {
    match expr.kind {
        ExprKind::Assign {
            ref name,
            ref value,
        } => visitor.visit_assign(name, value, &expr.id),
        ExprKind::Binary {
            ref left,
            ref operator,
            ref right,
        } => visitor.visit_binary(left, operator, right, &expr.id),
        ExprKind::Call {
            ref callee,
            ref arguments,
        } => visitor.visit_call(callee, arguments, &expr.id),
        ExprKind::Get {
            ref object,
            ref name,
        } => visitor.visit_get(object, name, &expr.id),
        ExprKind::Grouping {
            expr: ref inner_expr,
        } => visitor.visit_grouping(inner_expr, &expr.id),
        ExprKind::Literal(ref literal) => visitor.visit_literal(literal, &expr.id),
        ExprKind::Logical {
            ref left,
            ref operator,
            ref right,
        } => visitor.visit_logical(left, operator, right, &expr.id),
        ExprKind::Set {
            ref object,
            ref name,
            ref value,
        } => visitor.visit_set(object, name, value, &expr.id),
        ExprKind::Super { ref method } => visitor.visit_super(method, &expr.id),
        ExprKind::This => visitor.visit_this(&expr.id),
        ExprKind::Unary {
            ref operator,
            ref right,
        } => visitor.visit_unary(operator, right, &expr.id),
        ExprKind::Variable(ref name) => visitor.visit_variable(name, &expr.id),
    }
}

pub fn walk_function<V: Visitor>(visitor: &mut V, function: &Function) {
    visitor.visit_name(&function.name);
    walk_list!(visitor, visit_function_param, &function.params);
    walk_list!(visitor, visit_stmt, &function.body);
}

pub fn walk_if<V: Visitor>(
    visitor: &mut V,
    condition: &Expr,
    then_branch: &Stmt,
    else_branch: &Option<Box<Stmt>>,
    node_id: &NodeId,
) {
    visitor.visit_expr(condition, node_id);
    visitor.visit_stmt(then_branch);
    if let Some(else_branch) = else_branch {
        visitor.visit_stmt(else_branch);
    }
}

pub fn walk_print<V: Visitor>(visitor: &mut V, expr: &Expr, node_id: &NodeId) {
    visitor.visit_expr(expr, node_id);
}

pub fn walk_return<V: Visitor>(visitor: &mut V, expr: &Option<Expr>, node_id: &NodeId) {
    if let Some(expr) = expr {
        visitor.visit_expr(expr, node_id);
    }
}

pub fn walk_var<V: Visitor>(
    visitor: &mut V,
    name: &str,
    initializer: &Option<Expr>,
    node_id: &NodeId,
) {
    visitor.visit_name(name);
    if let Some(initializer) = initializer {
        visitor.visit_expr(initializer, node_id);
    }
}

pub fn walk_while<V: Visitor>(visitor: &mut V, condition: &Expr, body: &Stmt, node_id: &NodeId) {
    visitor.visit_expr(condition, node_id);
    visitor.visit_stmt(body);
}

pub fn walk_assign<V: Visitor>(visitor: &mut V, name: &str, value: &Expr, node_id: &NodeId) {
    visitor.visit_name(name);
    visitor.visit_expr(value, node_id);
}

pub fn walk_binary<V: Visitor>(
    visitor: &mut V,
    left: &Expr,
    _operator: &BinaryOp,
    right: &Expr,
    node_id: &NodeId,
) {
    visitor.visit_expr(left, node_id);
    visitor.visit_expr(right, node_id);
}

pub fn walk_call<V: Visitor>(visitor: &mut V, callee: &Expr, arguments: &[Expr], node_id: &NodeId) {
    visitor.visit_expr(callee, node_id);
    walk_list!(visitor, visit_expr, arguments, node_id);
}

pub fn walk_get<V: Visitor>(visitor: &mut V, object: &Expr, name: &str, node_id: &NodeId) {
    visitor.visit_expr(object, node_id);
    visitor.visit_name(name);
}

pub fn walk_grouping<V: Visitor>(visitor: &mut V, expr: &Expr, node_id: &NodeId) {
    visitor.visit_expr(expr, node_id);
}

pub fn walk_logical<V: Visitor>(
    visitor: &mut V,
    left: &Expr,
    _operator: &LogicalOp,
    right: &Expr,
    node_id: &NodeId,
) {
    visitor.visit_expr(left, node_id);
    visitor.visit_expr(right, node_id);
}

pub fn walk_set<V: Visitor>(
    visitor: &mut V,
    object: &Expr,
    name: &str,
    value: &Expr,
    node_id: &NodeId,
) {
    visitor.visit_expr(object, node_id);
    visitor.visit_name(name);
    visitor.visit_expr(value, node_id);
}

pub fn walk_super<V: Visitor>(visitor: &mut V, method: &str, _node_id: &NodeId) {
    visitor.visit_name(method);
}

pub fn walk_unary<V: Visitor>(
    visitor: &mut V,
    _operator: &UnaryOp,
    right: &Expr,
    node_id: &NodeId,
) {
    visitor.visit_expr(right, node_id);
}

pub fn walk_variable<V: Visitor>(visitor: &mut V, name: &str) {
    visitor.visit_name(name);
}
