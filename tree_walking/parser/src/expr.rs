use crate::node_id::NodeId;

#[derive(Clone, Debug, PartialEq)]
pub struct Expr {
    pub id: NodeId,
    pub kind: ExprKind,
    pub line: usize,
}

#[derive(Clone, Debug, PartialEq)]
pub enum ExprKind {
    Assign {
        name: String,
        value: Box<Expr>,
    },
    Binary {
        left: Box<Expr>,
        operator: BinaryOp,
        right: Box<Expr>,
    },
    Call {
        callee: Box<Expr>,
        arguments: Vec<Expr>,
    },
    Get {
        object: Box<Expr>,
        name: String,
    },
    Grouping {
        expr: Box<Expr>,
    },
    Literal(LiteralKind),
    Logical {
        left: Box<Expr>,
        operator: LogicalOp,
        right: Box<Expr>,
    },
    Set {
        object: Box<Expr>,
        name: String,
        value: Box<Expr>,
    },
    Super {
        method: String,
    },
    This,
    Unary {
        operator: UnaryOp,
        right: Box<Expr>,
    },
    Variable(String),
}

#[derive(Clone, Debug, PartialEq)]
pub enum BinaryOp {
    BangEqual,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,
    Minus,
    Plus,
    Star,
    Slash,
}

#[derive(Clone, Debug, PartialEq)]
pub enum LogicalOp {
    And,
    Or,
}

#[derive(Clone, Debug, PartialEq)]
pub enum UnaryOp {
    Bang,
    Minus,
}

#[derive(Clone, Debug, PartialEq)]
pub enum LiteralKind {
    Nil,
    False,
    True,
    Number(f64),
    String(String),
}

#[cfg(test)]
impl Expr {
    pub fn test_eq(&self, other: &Expr) -> bool {
        self.line == other.line && self.kind.test_eq(&other.kind)
    }
}
#[cfg(test)]
impl ExprKind {
    pub fn test_eq(&self, other: &ExprKind) -> bool {
        match (self, other) {
            (
                ExprKind::Assign { name, value },
                ExprKind::Assign {
                    name: other_name,
                    value: other_value,
                },
            ) => name == other_name && value.test_eq(other_value),
            (
                ExprKind::Binary {
                    left,
                    operator,
                    right,
                },
                ExprKind::Binary {
                    left: other_left,
                    operator: other_operator,
                    right: other_right,
                },
            ) => {
                left.test_eq(other_left) && operator == other_operator && right.test_eq(other_right)
            }
            (
                ExprKind::Call { callee, arguments },
                ExprKind::Call {
                    callee: other_callee,
                    arguments: other_arguments,
                },
            ) => {
                callee.test_eq(other_callee)
                    && arguments.len() == other_arguments.len()
                    && arguments
                        .iter()
                        .zip(other_arguments)
                        .all(|(arg, other_arg)| arg.test_eq(other_arg))
            }
            (ExprKind::Grouping { expr }, ExprKind::Grouping { expr: other_expr }) => {
                expr.test_eq(other_expr)
            }
            (ExprKind::Literal(lit), ExprKind::Literal(other_lit)) => lit == other_lit,
            (
                ExprKind::Logical {
                    left,
                    operator,
                    right,
                },
                ExprKind::Logical {
                    left: other_left,
                    operator: other_operator,
                    right: other_right,
                },
            ) => {
                left.test_eq(other_left) && operator == other_operator && right.test_eq(other_right)
            }
            (
                ExprKind::Unary { operator, right },
                ExprKind::Unary {
                    operator: other_operator,
                    right: other_right,
                },
            ) => operator == other_operator && right.test_eq(other_right),
            (ExprKind::Variable(name), ExprKind::Variable(other_name)) => name == other_name,
            _ => false,
        }
    }
}

#[cfg(test)]
pub fn assert_expr_eq(lhs: &Expr, rhs: &Expr) {
    assert!(
        lhs.test_eq(rhs),
        "\n  Left: {:?} \n  Right: {:?}\n",
        lhs,
        rhs
    );
}
