pub mod expr;
pub mod node_id;
pub mod stmt;

mod parser_error;
mod resolver;
mod visitor;

pub use resolver::Resolver;

use {
    expr::{BinaryOp, Expr, ExprKind, LiteralKind, LogicalOp, UnaryOp},
    node_id::NodeIdSource,
    parser_error::{ParserError, ParserErrors},
    scanner::token::{Token, TokenKind},
    std::{iter::Peekable, mem},
    stmt::{Function, Stmt, StmtKind},
};

pub struct Parser<T>
where
    T: Iterator<Item = Token>,
{
    tokens: Peekable<T>,
    errors: ParserErrors,
    node_id_source: NodeIdSource,
}

impl<T> Parser<T>
where
    T: Iterator<Item = Token>,
{
    pub fn new(tokens: T) -> Self {
        Self {
            tokens: tokens.peekable(),
            errors: ParserErrors::default(),
            node_id_source: NodeIdSource::default(),
        }
    }

    pub fn parse(&mut self) -> Result<Vec<Stmt>, ParserErrors> {
        let mut stmts = Vec::new();

        while !self.is_at_end() {
            match self.declaration() {
                Ok(stmt) => stmts.push(stmt),
                Err(err) => {
                    self.errors.add(err);
                    self.synchronize();
                }
            }
        }

        let errors = mem::take(&mut self.errors);
        if errors.any() {
            Err(errors)
        } else {
            Ok(stmts)
        }
    }

    fn declaration(&mut self) -> parser_error::Result<Stmt> {
        match self.tokens.peek().map(|token| &token.kind) {
            Some(TokenKind::Class) => self.class_declaration(),
            Some(TokenKind::Fun) => self.fun_declaration(),
            Some(TokenKind::Var) => self.var_declaration(),
            _ => self.statement(),
        }
    }

    fn class_declaration(&mut self) -> parser_error::Result<Stmt> {
        let _class = self.tokens.next();

        let name = match self.tokens.next() {
            Some(Token {
                kind: TokenKind::Identifier(name),
                ..
            }) => name,
            token => return Err(ParserError::new(token, "Expect class name.")),
        };

        let superclass = self
            .tokens
            .next_if(|token| matches!(token.kind, TokenKind::Less))
            .map(|_| match self.tokens.next() {
                Some(Token {
                    kind: TokenKind::Identifier(superclass),
                    line,
                }) => Ok((superclass, line)),
                token => Err(ParserError::new(token, "Expect superclass name")),
            })
            .transpose()?;

        self.consume(
            |kind| matches!(kind, TokenKind::LeftBrace),
            "Expect '{' before class body.",
        )?;

        let mut methods = Vec::new();
        while self
            .tokens
            .peek()
            .map(|token| !matches!(token.kind, TokenKind::RightBrace))
            .unwrap_or(false)
        {
            methods.push(self.function("method")?);
        }

        self.consume(
            |kind| matches!(kind, TokenKind::RightBrace),
            "Expect '}' after class body.",
        )?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::Class {
                name,
                superclass,
                methods,
            },
        })
    }

    fn fun_declaration(&mut self) -> parser_error::Result<Stmt> {
        let _fun = self.tokens.next();

        let function = self.function("function")?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::Function(function),
        })
    }

    fn function(&mut self, kind: &str) -> parser_error::Result<Function> {
        let name = match self.tokens.next() {
            Some(Token {
                kind: TokenKind::Identifier(id),
                ..
            }) => id,
            token => return Err(ParserError::new(token, format!("Expect {} name.", kind))),
        };

        self.consume(
            |kind| matches!(kind, TokenKind::LeftParen),
            format!("Expect '(' after {} name.", kind),
        )?;

        let mut params = Vec::new();
        let mut param_count = 0;
        if self
            .tokens
            .peek()
            .map(|token| !matches!(token.kind, TokenKind::RightParen))
            .unwrap_or(false)
        {
            loop {
                if param_count >= 255 {
                    self.errors.add(ParserError::new(
                        self.tokens.peek().cloned(),
                        "Can't have more than 255 parameters.",
                    ));
                }

                let name = match self.tokens.next() {
                    Some(Token {
                        kind: TokenKind::Identifier(param),
                        ..
                    }) => param,
                    token => return Err(ParserError::new(token, "Expect parameter name.")),
                };
                params.push(name);
                param_count += 1;

                if self
                    .tokens
                    .peek()
                    .map(|token| !matches!(token.kind, TokenKind::Comma))
                    .unwrap_or(true)
                {
                    break;
                }
                let _comma = self.tokens.next();
            }
        }

        self.consume(
            |kind| matches!(kind, TokenKind::RightParen),
            "Expect ')' after arguments.",
        )?;

        if self
            .tokens
            .peek()
            .map(|token| !matches!(token.kind, TokenKind::LeftBrace))
            .unwrap_or(true)
        {
            return Err(ParserError::new(
                self.tokens.next(),
                format!("Expect '{{' before {} body", kind),
            ));
        }
        let body = match self.block()?.kind {
            StmtKind::Block(body) => body,
            _ => return Err(ParserError::new(None, "Expected function body.")),
        };

        Ok(Function { name, params, body })
    }

    fn var_declaration(&mut self) -> parser_error::Result<Stmt> {
        let _var = self.tokens.next();

        let name = match self.tokens.next() {
            Some(Token {
                kind: TokenKind::Identifier(id),
                ..
            }) => id,
            token => return Err(ParserError::new(token, "Expect variable name.")),
        };

        let initializer = self
            .tokens
            .next_if(|token| matches!(token.kind, TokenKind::Equal))
            .map(|_| self.expression())
            .transpose()?;

        self.consume(
            |kind| matches!(kind, TokenKind::SemiColon),
            "Expect ';' after variable declaration.",
        )?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::Var { name, initializer },
        })
    }

    fn statement(&mut self) -> parser_error::Result<Stmt> {
        match self.tokens.peek().map(|token| &token.kind) {
            Some(TokenKind::For) => self.for_statement(),
            Some(TokenKind::If) => self.if_statement(),
            Some(TokenKind::Print) => self.print_statement(),
            Some(TokenKind::Return) => self.return_statement(),
            Some(TokenKind::While) => self.while_statement(),
            Some(TokenKind::LeftBrace) => self.block(),
            _ => self.expression_statement(),
        }
    }

    fn for_statement(&mut self) -> parser_error::Result<Stmt> {
        let _for = self.tokens.next();
        self.consume(
            |kind| matches!(kind, TokenKind::LeftParen),
            "Expect '(' after 'for'.",
        )?;
        let initializer = match self.tokens.peek().map(|token| &token.kind) {
            Some(TokenKind::SemiColon) => {
                let _semi_colon = self.tokens.next();
                None
            }
            Some(TokenKind::Var) => Some(self.var_declaration()?),
            _ => Some(self.expression_statement()?),
        };

        let condition = match self.tokens.peek().map(|token| &token.kind) {
            Some(TokenKind::SemiColon) => None,
            _ => Some(self.expression()?),
        };
        let condition_semi_colon = self.consume(
            |kind| matches!(kind, TokenKind::SemiColon),
            "Expect ';' after loop condition.",
        )?;

        let increment = match self.tokens.peek().map(|token| &token.kind) {
            Some(TokenKind::RightParen) => None,
            _ => Some(self.expression()?),
        };
        self.consume(
            |kind| matches!(kind, TokenKind::RightParen),
            "Expect ')' after for clauses.",
        )?;

        let mut body = self.statement()?;

        if let Some(increment) = increment {
            body = Stmt {
                id: self.node_id_source.get_id(),
                kind: StmtKind::Block(vec![
                    body,
                    Stmt {
                        id: self.node_id_source.get_id(),
                        kind: StmtKind::Expr(increment),
                    },
                ]),
            }
        }

        let condition = condition.unwrap_or(Expr {
            id: self.node_id_source.get_id(),
            kind: ExprKind::Literal(LiteralKind::True),
            line: condition_semi_colon.line,
        });
        body = Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::While {
                condition,
                body: Box::new(body),
            },
        };

        if let Some(initializer) = initializer {
            body = Stmt {
                id: self.node_id_source.get_id(),
                kind: StmtKind::Block(vec![initializer, body]),
            }
        }

        Ok(body)
    }

    fn if_statement(&mut self) -> parser_error::Result<Stmt> {
        let _if = self.tokens.next();
        self.consume(
            |kind| matches!(kind, TokenKind::LeftParen),
            "Expect '(' after 'if'.",
        )?;
        let condition = self.expression()?;
        self.consume(
            |kind| matches!(kind, TokenKind::RightParen),
            "Expect ')' after if condition.",
        )?;

        let then_branch = Box::new(self.statement()?);
        let else_branch = if self
            .tokens
            .next_if(|token| matches!(token.kind, TokenKind::Else))
            .is_some()
        {
            Some(Box::new(self.statement()?))
        } else {
            None
        };

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::If {
                condition,
                then_branch,
                else_branch,
            },
        })
    }

    fn print_statement(&mut self) -> parser_error::Result<Stmt> {
        let _print = self.tokens.next();
        let value = self.expression()?;

        self.consume(
            |kind| matches!(kind, TokenKind::SemiColon),
            "Expect ';' after value.",
        )?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::Print(value),
        })
    }

    fn return_statement(&mut self) -> parser_error::Result<Stmt> {
        let _return = self.tokens.next();

        let value = if self
            .tokens
            .peek()
            .map(|token| !matches!(token.kind, TokenKind::SemiColon))
            .unwrap_or(false)
        {
            Some(self.expression()?)
        } else {
            None
        };

        self.consume(
            |kind| matches!(kind, TokenKind::SemiColon),
            "Expect ';' after return value.",
        )?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::Return(value),
        })
    }

    fn while_statement(&mut self) -> parser_error::Result<Stmt> {
        let _while = self.tokens.next();
        self.consume(
            |kind| matches!(kind, TokenKind::LeftParen),
            "Expect '(' after 'while'.",
        )?;
        let condition = self.expression()?;
        self.consume(
            |kind| matches!(kind, TokenKind::RightParen),
            "Expect ')' after condition.",
        )?;
        let body = self.statement()?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::While {
                condition,
                body: Box::new(body),
            },
        })
    }

    fn block(&mut self) -> parser_error::Result<Stmt> {
        let _left_brace = self.tokens.next();
        let mut stmts = Vec::new();
        while self
            .tokens
            .peek()
            .map(|token| !matches!(token.kind, TokenKind::RightBrace | TokenKind::Eof))
            .unwrap_or(false)
        {
            stmts.push(self.declaration()?);
        }

        self.consume(
            |kind| matches!(kind, TokenKind::RightBrace),
            "Expect '}' after block.",
        )?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::Block(stmts),
        })
    }

    fn expression_statement(&mut self) -> parser_error::Result<Stmt> {
        let expr = self.expression()?;

        self.consume(
            |kind| matches!(kind, TokenKind::SemiColon),
            "Expect ';' after expression.",
        )?;

        Ok(Stmt {
            id: self.node_id_source.get_id(),
            kind: StmtKind::Expr(expr),
        })
    }

    fn expression(&mut self) -> parser_error::Result<Expr> {
        self.assignment()
    }

    fn assignment(&mut self) -> parser_error::Result<Expr> {
        let expr = self.logical_or()?;

        if let Some(equals) = self
            .tokens
            .next_if(|token| matches!(token.kind, TokenKind::Equal))
        {
            let value = self.assignment()?;

            if let ExprKind::Variable(name) = expr.kind {
                return Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Assign {
                        name,
                        value: Box::new(value),
                    },
                    line: expr.line,
                });
            } else if let ExprKind::Get { object, name } = expr.kind {
                return Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Set {
                        object,
                        name,
                        value: Box::new(value),
                    },
                    line: expr.line,
                });
            }

            self.errors
                .add(ParserError::new(Some(equals), "Invalid assinment target."));
        }

        Ok(expr)
    }

    fn logical_or(&mut self) -> parser_error::Result<Expr> {
        let mut expr = self.logical_and()?;

        while let Some((operator, line)) = self.tokens.peek().and_then(|token| {
            match token.kind {
                TokenKind::Or => Some(LogicalOp::Or),
                _ => None,
            }
            .map(|op| (op, token.line))
        }) {
            let _token = self.tokens.next();
            let right = self.logical_and()?;

            expr = Expr {
                id: self.node_id_source.get_id(),
                kind: ExprKind::Logical {
                    left: Box::new(expr),
                    operator,
                    right: Box::new(right),
                },
                line,
            }
        }

        Ok(expr)
    }

    fn logical_and(&mut self) -> parser_error::Result<Expr> {
        let mut expr = self.equality()?;

        while let Some((operator, line)) = self.tokens.peek().and_then(|token| {
            match token.kind {
                TokenKind::And => Some(LogicalOp::And),
                _ => None,
            }
            .map(|op| (op, token.line))
        }) {
            let _token = self.tokens.next();
            let right = self.equality()?;

            expr = Expr {
                id: self.node_id_source.get_id(),
                kind: ExprKind::Logical {
                    left: Box::new(expr),
                    operator,
                    right: Box::new(right),
                },
                line,
            }
        }

        Ok(expr)
    }

    fn equality(&mut self) -> parser_error::Result<Expr> {
        let mut expr = self.comparison()?;

        while let Some((operator, line)) = self.tokens.peek().and_then(|token| {
            match token.kind {
                TokenKind::BangEqual => Some(BinaryOp::BangEqual),
                TokenKind::EqualEqual => Some(BinaryOp::EqualEqual),
                _ => None,
            }
            .map(|op| (op, token.line))
        }) {
            let _token = self.tokens.next();
            let right = self.comparison()?;

            expr = Expr {
                id: self.node_id_source.get_id(),
                kind: ExprKind::Binary {
                    left: Box::new(expr),
                    operator,
                    right: Box::new(right),
                },
                line,
            }
        }

        Ok(expr)
    }

    fn comparison(&mut self) -> parser_error::Result<Expr> {
        let mut expr = self.term()?;

        while let Some((operator, line)) = self.tokens.peek().and_then(|token| {
            match token.kind {
                TokenKind::Greater => Some(BinaryOp::Greater),
                TokenKind::GreaterEqual => Some(BinaryOp::GreaterEqual),
                TokenKind::Less => Some(BinaryOp::Less),
                TokenKind::LessEqual => Some(BinaryOp::LessEqual),
                _ => None,
            }
            .map(|op| (op, token.line))
        }) {
            let _token = self.tokens.next();
            let right = self.term()?;

            expr = Expr {
                id: self.node_id_source.get_id(),
                kind: ExprKind::Binary {
                    left: Box::new(expr),
                    operator,
                    right: Box::new(right),
                },
                line,
            }
        }

        Ok(expr)
    }

    fn term(&mut self) -> parser_error::Result<Expr> {
        let mut expr = self.factor()?;

        while let Some((operator, line)) = self.tokens.peek().and_then(|token| {
            match token.kind {
                TokenKind::Minus => Some(BinaryOp::Minus),
                TokenKind::Plus => Some(BinaryOp::Plus),
                _ => None,
            }
            .map(|op| (op, token.line))
        }) {
            let _token = self.tokens.next();
            let right = self.factor()?;

            expr = Expr {
                id: self.node_id_source.get_id(),
                kind: ExprKind::Binary {
                    left: Box::new(expr),
                    operator,
                    right: Box::new(right),
                },
                line,
            }
        }

        Ok(expr)
    }

    fn factor(&mut self) -> parser_error::Result<Expr> {
        let mut expr = self.unary()?;

        while let Some((operator, line)) = self.tokens.peek().and_then(|token| {
            match token.kind {
                TokenKind::Slash => Some(BinaryOp::Slash),
                TokenKind::Star => Some(BinaryOp::Star),
                _ => None,
            }
            .map(|op| (op, token.line))
        }) {
            let _token = self.tokens.next();
            let right = self.unary()?;

            expr = Expr {
                id: self.node_id_source.get_id(),
                kind: ExprKind::Binary {
                    left: Box::new(expr),
                    operator,
                    right: Box::new(right),
                },
                line,
            }
        }

        Ok(expr)
    }

    fn unary(&mut self) -> parser_error::Result<Expr> {
        if let Some((operator, line)) = self.tokens.peek().and_then(|token| {
            match token.kind {
                TokenKind::Bang => Some(UnaryOp::Bang),
                TokenKind::Minus => Some(UnaryOp::Minus),
                _ => None,
            }
            .map(|op| (op, token.line))
        }) {
            let _token = self.tokens.next();
            let right = self.unary()?;

            Ok(Expr {
                id: self.node_id_source.get_id(),
                kind: ExprKind::Unary {
                    operator,
                    right: Box::new(right),
                },
                line,
            })
        } else {
            self.call()
        }
    }

    fn call(&mut self) -> parser_error::Result<Expr> {
        let mut expr = self.primary()?;

        loop {
            if self
                .tokens
                .next_if(|token| matches!(token.kind, TokenKind::LeftParen))
                .is_some()
            {
                expr = self.finish_call(expr)?;
            } else if self
                .tokens
                .next_if(|token| matches!(token.kind, TokenKind::Dot))
                .is_some()
            {
                let (name, line) = match self.tokens.next() {
                    Some(Token {
                        kind: TokenKind::Identifier(name),
                        line,
                    }) => (name, line),
                    token => {
                        return Err(ParserError::new(token, "Expect property name after '.'."))
                    }
                };

                expr = Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Get {
                        object: Box::new(expr),
                        name,
                    },
                    line,
                };
            } else {
                break;
            }
        }

        Ok(expr)
    }

    fn finish_call(&mut self, callee: Expr) -> parser_error::Result<Expr> {
        let mut arguments = Vec::new();
        let mut arg_count = 0;
        if self
            .tokens
            .peek()
            .map(|token| !matches!(token.kind, TokenKind::RightParen))
            .unwrap_or(false)
        {
            loop {
                if arg_count >= 255 {
                    self.errors.add(ParserError::new(
                        self.tokens.peek().cloned(),
                        "Can't have more than 255 arguments.",
                    ));
                }

                let arg = self.expression()?;
                arguments.push(arg);
                arg_count += 1;

                if self
                    .tokens
                    .peek()
                    .map(|token| !matches!(token.kind, TokenKind::Comma))
                    .unwrap_or(true)
                {
                    break;
                }
                let _comma = self.tokens.next();
            }
        }

        let paren = self.consume(
            |kind| matches!(kind, TokenKind::RightParen),
            "Expect ')' after arguments.",
        )?;

        Ok(Expr {
            id: self.node_id_source.get_id(),
            kind: ExprKind::Call {
                callee: Box::new(callee),
                arguments,
            },
            line: paren.line,
        })
    }

    fn primary(&mut self) -> parser_error::Result<Expr> {
        if let Some(token) = self.tokens.next() {
            match token.kind {
                TokenKind::Nil => Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Literal(LiteralKind::Nil),
                    line: token.line,
                }),
                TokenKind::False => Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Literal(LiteralKind::False),
                    line: token.line,
                }),
                TokenKind::True => Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Literal(LiteralKind::True),
                    line: token.line,
                }),
                TokenKind::Number(number) => Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Literal(LiteralKind::Number(number)),
                    line: token.line,
                }),
                TokenKind::String(string) => Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Literal(LiteralKind::String(string)),
                    line: token.line,
                }),
                TokenKind::Super => {
                    self.consume(
                        |kind| matches!(kind, TokenKind::Dot),
                        "Expect '.' after 'super'.",
                    )?;

                    let method = match self.tokens.next() {
                        Some(Token {
                            kind: TokenKind::Identifier(method),
                            ..
                        }) => method,
                        token => {
                            return Err(ParserError::new(token, "Expect superclass method name."))
                        }
                    };

                    Ok(Expr {
                        id: self.node_id_source.get_id(),
                        kind: ExprKind::Super { method },
                        line: token.line,
                    })
                }
                TokenKind::This => Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::This,
                    line: token.line,
                }),
                TokenKind::Identifier(name) => Ok(Expr {
                    id: self.node_id_source.get_id(),
                    kind: ExprKind::Variable(name),
                    line: token.line,
                }),
                TokenKind::LeftParen => {
                    let expr = self.expression()?;

                    self.consume(
                        |kind| matches!(kind, TokenKind::RightParen),
                        "Expect ')' after expression.",
                    )?;

                    Ok(Expr {
                        id: self.node_id_source.get_id(),
                        kind: ExprKind::Grouping {
                            expr: Box::new(expr),
                        },
                        line: token.line,
                    })
                }
                _ => Err(ParserError::new(Some(token), "Expect expression.")),
            }
        } else {
            Err(ParserError::new(None, "Expect expression."))
        }
    }

    fn synchronize(&mut self) {
        while let Some(token) = self.tokens.next_if(|token| {
            !matches!(
                token.kind,
                TokenKind::Class
                    | TokenKind::Fun
                    | TokenKind::Var
                    | TokenKind::For
                    | TokenKind::If
                    | TokenKind::While
                    | TokenKind::Print
                    | TokenKind::Return
            )
        }) {
            if token.kind == TokenKind::SemiColon {
                break;
            }
        }
    }

    fn consume(
        &mut self,
        predicate: impl Fn(&TokenKind) -> bool,
        message: impl Into<String>,
    ) -> parser_error::Result<Token> {
        let token = self.tokens.next();

        match token {
            Some(token) if predicate(&token.kind) => Ok(token),
            token => Err(ParserError::new(token, message)),
        }
    }

    fn is_at_end(&mut self) -> bool {
        self.tokens
            .peek()
            .map(|token| matches!(token.kind, TokenKind::Eof))
            .unwrap_or(true)
    }
}

#[cfg(test)]
mod tests {
    use {super::*, scanner::Scanner};

    #[test]
    fn new_parser() {
        let tokens: Vec<Token> = Vec::new();

        let _parser = Parser::new(tokens.into_iter());
    }

    #[test]
    fn parse_expressions() {
        let scanner = Scanner::new("(1 + 1)");
        let mut parser = Parser::new(scanner);
        let mut node_id_source = NodeIdSource::default();

        let expr = parser.expression();

        let expr = expr.unwrap();
        let expected = Expr {
            id: node_id_source.get_id(),
            kind: ExprKind::Grouping {
                expr: Box::new(Expr {
                    id: node_id_source.get_id(),
                    kind: ExprKind::Binary {
                        left: Box::new(Expr {
                            id: node_id_source.get_id(),
                            kind: ExprKind::Literal(LiteralKind::Number(1.0)),
                            line: 1,
                        }),
                        operator: BinaryOp::Plus,
                        right: Box::new(Expr {
                            id: node_id_source.get_id(),
                            kind: ExprKind::Literal(LiteralKind::Number(1.0)),
                            line: 1,
                        }),
                    },
                    line: 1,
                }),
            },
            line: 1,
        };
        expr::assert_expr_eq(&expr, &expected);
    }
}
