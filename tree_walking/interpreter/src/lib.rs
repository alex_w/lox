mod callable;
mod environment;
mod instance;
mod object;
mod runtime_error;

use {
    callable::{Callable, Class, Function},
    environment::Environment,
    object::Object,
    parser::{
        expr::{BinaryOp, Expr, ExprKind, LiteralKind, LogicalOp, UnaryOp},
        node_id::NodeId,
        stmt::{self, Stmt, StmtKind},
    },
    runtime_error::RuntimeError,
    std::{collections::HashMap, rc::Rc},
};

pub struct Interpreter {
    environment: Environment,
    globals: Environment,
}

impl Default for Interpreter {
    fn default() -> Self {
        let globals = Environment::new_global();

        Self {
            environment: globals.clone(),
            globals,
        }
    }
}

impl Interpreter {
    fn new(environment: Environment, globals: Environment) -> Self {
        Self {
            environment,
            globals,
        }
    }

    fn new_enclosed(&self) -> Self {
        Self {
            environment: Environment::new_enclosed(self.environment.clone()),
            globals: self.globals.clone(),
        }
    }

    pub fn interpret(
        &mut self,
        stmts: impl IntoIterator<Item = Stmt>,
        resolutions: HashMap<NodeId, usize>,
    ) -> runtime_error::Result<()> {
        let resolutions = Rc::new(resolutions);
        for stmt in stmts {
            self.execute(stmt, resolutions.clone())?;
        }

        Ok(())
    }

    fn execute(
        &mut self,
        stmt: Stmt,
        resolutions: Rc<HashMap<NodeId, usize>>,
    ) -> runtime_error::Result<Option<Object>> {
        let value = match stmt.kind {
            StmtKind::Block(stmts) => self.execute_block(stmts, resolutions)?,
            StmtKind::Class {
                name,
                superclass,
                methods,
            } => {
                self.execute_class(stmt.id, name, superclass, methods, resolutions)?;
                None
            }
            StmtKind::Expr(expr) => {
                self.evaluate(expr, &resolutions)?;
                None
            }
            StmtKind::Function(stmt::Function { name, params, body }) => {
                self.execute_function(name, params, body, resolutions)?;
                None
            }
            StmtKind::If {
                condition,
                then_branch,
                else_branch,
            } => self.execute_if(
                condition,
                *then_branch,
                else_branch.map(|s| *s),
                resolutions,
            )?,
            StmtKind::Print(expr) => {
                self.execute_print(expr, &resolutions)?;
                None
            }
            StmtKind::Return(value) => self.execute_return(value, &resolutions)?,
            StmtKind::Var { name, initializer } => {
                self.execute_declaration(name, initializer, &resolutions)?;
                None
            }
            StmtKind::While { condition, body } => {
                self.execute_while(condition, *body, resolutions)?
            }
        };

        Ok(value)
    }

    fn execute_class(
        &mut self,
        id: NodeId,
        name: String,
        superclass: Option<(String, usize)>,
        methods: Vec<stmt::Function>,
        resolutions: Rc<HashMap<NodeId, usize>>,
    ) -> runtime_error::Result<()> {
        let superclass = superclass
            .and_then(|(superclass, line)| {
                self.get_variable(id, &superclass, &resolutions).map(
                    |superclass| match superclass {
                        Object::Class(class) => Ok(class),
                        _ => Err(RuntimeError::new("Superclass must be a class.", line)),
                    },
                )
            })
            .transpose()?;

        let super_environment = superclass.clone().map(|superclass| {
            let mut environment = Environment::new_enclosed(self.environment.clone());
            environment.define(String::from("super"), Object::Class(superclass));

            environment
        });
        let methods = methods
            .into_iter()
            .map(|method| {
                let is_initializer = method.name == "init";
                (
                    method.name.clone(),
                    Function::new(
                        method.name,
                        method.params,
                        method.body,
                        is_initializer,
                        super_environment
                            .clone()
                            .unwrap_or_else(|| self.environment.clone()),
                        self.globals.clone(),
                        resolutions.clone(),
                    ),
                )
            })
            .collect();

        let class = Object::Class(Class::new(name.clone(), superclass, methods));
        self.environment.define(name, class);

        Ok(())
    }

    fn execute_function(
        &mut self,
        name: String,
        params: Vec<String>,
        body: Vec<Stmt>,
        resolutions: Rc<HashMap<NodeId, usize>>,
    ) -> runtime_error::Result<()> {
        let function = Object::Callable(Rc::new(Function::new(
            name.clone(),
            params,
            body,
            false,
            self.environment.clone(),
            self.globals.clone(),
            resolutions,
        )));
        self.environment.define(name, function);

        Ok(())
    }

    fn execute_if(
        &mut self,
        condition: Expr,
        then_branch: Stmt,
        else_branch: Option<Stmt>,
        resolutions: Rc<HashMap<NodeId, usize>>,
    ) -> runtime_error::Result<Option<Object>> {
        if self.evaluate(condition, &resolutions)?.is_truthy() == Object::Boolean(true) {
            self.execute(then_branch, resolutions)
        } else if let Some(else_branch) = else_branch {
            self.execute(else_branch, resolutions)
        } else {
            Ok(None)
        }
    }

    fn execute_block(
        &mut self,
        stmts: Vec<Stmt>,
        resolutions: Rc<HashMap<NodeId, usize>>,
    ) -> runtime_error::Result<Option<Object>> {
        let mut interpreter = self.new_enclosed();
        for stmt in stmts {
            if let Some(value) = interpreter.execute(stmt, resolutions.clone())? {
                return Ok(Some(value));
            }
        }

        Ok(None)
    }

    fn execute_print(
        &mut self,
        expr: Expr,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<()> {
        let obj = self.evaluate(expr, resolutions)?;

        println!("{}", obj);

        Ok(())
    }

    fn execute_return(
        &mut self,
        value: Option<Expr>,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Option<Object>> {
        if let Some(expr) = value {
            let obj = self.evaluate(expr, resolutions)?;
            Ok(Some(obj))
        } else {
            Ok(Some(Object::Nil))
        }
    }

    fn execute_declaration(
        &mut self,
        name: String,
        initializer: Option<Expr>,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<()> {
        let value = match initializer {
            Some(expr) => self.evaluate(expr, resolutions)?,
            None => Object::Nil,
        };

        self.environment.define(name, value);

        Ok(())
    }

    fn execute_while(
        &mut self,
        condition: Expr,
        body: Stmt,
        resolutions: Rc<HashMap<NodeId, usize>>,
    ) -> runtime_error::Result<Option<Object>> {
        // TODO: Maybe a sign we should only borrow Expr. Could be odd that we consume the instructions.
        while self.evaluate(condition.clone(), &resolutions)?.is_truthy() == Object::Boolean(true) {
            if let Some(value) = self.execute(body.clone(), resolutions.clone())? {
                return Ok(Some(value));
            }
        }

        Ok(None)
    }

    fn evaluate(
        &mut self,
        expr: Expr,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        match expr.kind {
            ExprKind::Assign { name, value } => {
                self.evaluate_assign(expr.id, name, *value, resolutions)
            }
            ExprKind::Binary {
                left,
                operator,
                right,
            } => self.evaluate_binary(*left, operator, *right, expr.line, resolutions),
            ExprKind::Call { callee, arguments } => {
                self.evaluate_call(*callee, arguments, expr.line, resolutions)
            }
            ExprKind::Get { object, name } => {
                self.evaluate_get(*object, name, expr.line, resolutions)
            }
            ExprKind::Grouping { expr } => self.evaluate(*expr, resolutions),
            ExprKind::Literal(LiteralKind::Nil) => Ok(Object::Nil),
            ExprKind::Literal(LiteralKind::False) => Ok(Object::Boolean(false)),
            ExprKind::Literal(LiteralKind::True) => Ok(Object::Boolean(true)),
            ExprKind::Literal(LiteralKind::Number(n)) => Ok(Object::Number(n)),
            ExprKind::Literal(LiteralKind::String(s)) => Ok(Object::String(s)),
            ExprKind::Logical {
                left,
                operator,
                right,
            } => self.evaluate_logical(*left, operator, *right, resolutions),
            ExprKind::Set {
                object,
                name,
                value,
            } => self.evaluate_set(*object, name, *value, expr.line, resolutions),
            ExprKind::Super { method } => {
                self.evaluate_super(expr.id, method, expr.line, resolutions)
            }
            ExprKind::This => self.evaluate_this(expr.id, expr.line, resolutions),
            ExprKind::Unary { operator, right } => {
                self.evaluate_unary(operator, *right, expr.line, resolutions)
            }
            ExprKind::Variable(ref name) => {
                self.evaluate_variable(expr.id, name, expr.line, resolutions)
            }
        }
    }

    fn evaluate_assign(
        &mut self,
        id: NodeId,
        name: String,
        value: Expr,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let value = self.evaluate(value, resolutions)?;

        if let Some(distance) = resolutions.get(&id) {
            self.environment.assign_at(name, value.clone(), *distance);
        } else {
            self.globals.assign(name, value.clone());
        }

        Ok(value)
    }

    fn evaluate_binary(
        &mut self,
        left: Expr,
        operator: BinaryOp,
        right: Expr,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let left = self.evaluate(left, resolutions)?;
        let right = self.evaluate(right, resolutions)?;

        match operator {
            BinaryOp::Minus => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Number(l - r)),
                _ => Err(RuntimeError::new("Operands must be numbers.", line)),
            },
            BinaryOp::Plus => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Number(l + r)),
                (Object::String(mut l), Object::String(r)) => {
                    l.push_str(&r);

                    Ok(Object::String(l))
                }
                _ => Err(RuntimeError::new(
                    "Operands must be strings or numbers",
                    line,
                )),
            },
            BinaryOp::Slash => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Number(l / r)),
                _ => Err(RuntimeError::new("Operands must be numbers.", line)),
            },
            BinaryOp::Star => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Number(l * r)),
                _ => Err(RuntimeError::new("Operands must be numbers.", line)),
            },
            BinaryOp::Greater => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Boolean(l > r)),
                _ => Err(RuntimeError::new("Operands must be numbers.", line)),
            },
            BinaryOp::GreaterEqual => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Boolean(l >= r)),
                _ => Err(RuntimeError::new("Operands must be numbers.", line)),
            },
            BinaryOp::Less => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Boolean(l < r)),
                _ => Err(RuntimeError::new("Operands must be numbers.", line)),
            },
            BinaryOp::LessEqual => match (left, right) {
                (Object::Number(l), Object::Number(r)) => Ok(Object::Boolean(l <= r)),
                _ => Err(RuntimeError::new("Operands must be numbers.", line)),
            },
            BinaryOp::BangEqual => Ok(Object::Boolean(!(left == right))),
            BinaryOp::EqualEqual => Ok(Object::Boolean(left == right)),
        }
    }

    fn evaluate_call(
        &mut self,
        callee: Expr,
        arguments: Vec<Expr>,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        match self.evaluate(callee, resolutions)? {
            Object::Callable(callee) if callee.arity() != arguments.len() => {
                Err(RuntimeError::new(
                    format!(
                        "Expected {} arguments but got {}.",
                        callee.arity(),
                        arguments.len()
                    ),
                    line,
                ))
            }
            Object::Callable(callee) => {
                let arguments = arguments
                    .into_iter()
                    .map(|arg| self.evaluate(arg, resolutions))
                    .collect::<Result<Vec<Object>, _>>()?;

                callee.call(arguments, line)
            }
            Object::Class(class) if class.arity() != arguments.len() => Err(RuntimeError::new(
                format!(
                    "Expected {} arguments but got {}.",
                    class.arity(),
                    arguments.len()
                ),
                line,
            )),
            Object::Class(class) => {
                let arguments = arguments
                    .into_iter()
                    .map(|arg| self.evaluate(arg, resolutions))
                    .collect::<Result<Vec<Object>, _>>()?;

                class.call(arguments, line)
            }
            _ => Err(RuntimeError::new(
                "Can only call functions and classes.",
                line,
            )),
        }
    }

    fn evaluate_get(
        &mut self,
        object: Expr,
        name: String,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let object = self.evaluate(object, resolutions)?;

        match object {
            Object::Instance(instance) => instance
                .get(&name)
                .ok_or_else(|| RuntimeError::new(format!("Undefined property '{}'.", &name), line)),
            _ => Err(RuntimeError::new("Only instances have properties.", line)),
        }
    }

    fn evaluate_logical(
        &mut self,
        left: Expr,
        operator: LogicalOp,
        right: Expr,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let left = self.evaluate(left, resolutions)?;

        match (operator, left.is_truthy()) {
            (LogicalOp::Or, Object::Boolean(true)) => Ok(left),
            (LogicalOp::And, Object::Boolean(false)) => Ok(left),
            _ => self.evaluate(right, resolutions),
        }
    }

    fn evaluate_set(
        &mut self,
        object: Expr,
        name: String,
        value: Expr,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let object = self.evaluate(object, resolutions)?;

        match object {
            Object::Instance(mut instance) => {
                let value = self.evaluate(value, resolutions)?;

                instance.set(name, value.clone());

                Ok(value)
            }
            _ => Err(RuntimeError::new("Only instances have fields.", line)),
        }
    }

    fn evaluate_super(
        &mut self,
        id: NodeId,
        method: String,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let distance = resolutions
            .get(&id)
            .ok_or_else(|| RuntimeError::new("Undefined variable 'super'.", line))?;

        let superclass = match self.environment.get_at("super", *distance) {
            Some(Object::Class(class)) => class,
            _ => return Err(RuntimeError::new("Expected 'super' to be a class.", line)),
        };

        let this = match self.environment.get_at("this", distance - 1) {
            Some(Object::Instance(this)) => this,
            _ => {
                return Err(RuntimeError::new(
                    "Expected 'this' to be an instance.",
                    line,
                ))
            }
        };

        let method = superclass
            .get_method(&method)
            .ok_or_else(|| RuntimeError::new(format!("Undefined property '{}'.", method), line))?;

        Ok(Object::Callable(Rc::new(method.bind(this))))
    }

    fn evaluate_this(
        &mut self,
        id: NodeId,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        self.evaluate_variable(id, "this", line, resolutions)
    }

    fn evaluate_unary(
        &mut self,
        operator: UnaryOp,
        right: Expr,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let right = self.evaluate(right, resolutions)?;

        match operator {
            UnaryOp::Bang => Ok(right.not()),
            UnaryOp::Minus => match right {
                Object::Number(n) => Ok(Object::Number(-n)),
                _ => Err(RuntimeError::new("Operand must be a number.", line)),
            },
        }
    }

    fn evaluate_variable(
        &self,
        id: NodeId,
        name: &str,
        line: usize,
        resolutions: &HashMap<NodeId, usize>,
    ) -> runtime_error::Result<Object> {
        let variable = self.get_variable(id, name, resolutions);

        variable.ok_or_else(|| RuntimeError::new(format!("Undefined variable '{}'.", name), line))
    }

    fn get_variable(
        &self,
        id: NodeId,
        name: &str,
        resolutions: &HashMap<NodeId, usize>,
    ) -> Option<Object> {
        if let Some(distance) = resolutions.get(&id) {
            self.environment.get_at(name, *distance)
        } else {
            self.globals.get(name)
        }
    }
}
