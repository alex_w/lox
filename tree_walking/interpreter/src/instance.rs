use {
    crate::{callable::Class, Object},
    std::{cell::RefCell, collections::HashMap, fmt, rc::Rc},
};

#[derive(Clone, Debug)]
pub struct Instance {
    inner: Rc<RefCell<InstanceInner>>,
}

#[derive(Debug)]
struct InstanceInner {
    class: Class,
    fields: HashMap<String, Object>,
}

impl Instance {
    pub fn new(class: Class) -> Self {
        Self {
            inner: Rc::new(RefCell::new(InstanceInner {
                class,
                fields: HashMap::new(),
            })),
        }
    }

    pub fn get(&self, name: &str) -> Option<Object> {
        let inner = self.inner.borrow();

        inner.fields.get(name).cloned().or_else(|| {
            inner
                .class
                .get_method(name)
                .map(|method| Object::Callable(Rc::new(method.bind(self.clone()))))
        })
    }

    pub fn set(&mut self, name: String, value: Object) {
        self.inner.borrow_mut().fields.insert(name, value);
    }
}

impl fmt::Display for Instance {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "<instance:{}>", self.inner.borrow().class.name())
    }
}

impl PartialEq for Instance {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.inner, &other.inner)
    }
}
