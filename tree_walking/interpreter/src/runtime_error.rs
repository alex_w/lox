use std::{error::Error, fmt, result};

#[derive(Debug, PartialEq)]
pub struct RuntimeError {
    message: String,
    line: usize,
}

pub type Result<T> = result::Result<T, RuntimeError>;

impl RuntimeError {
    pub fn new(message: impl Into<String>, line: usize) -> Self {
        Self {
            message: message.into(),
            line,
        }
    }
}

impl fmt::Display for RuntimeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}, {}", self.line, self.message)
    }
}

impl Error for RuntimeError {}
