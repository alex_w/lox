mod class;
mod clock;
mod function;

pub use {class::Class, clock::Clock, function::Function};

use {
    crate::{object::Object, runtime_error},
    std::fmt::{self, Debug},
};

pub trait Callable: fmt::Display + Debug {
    fn call(&self, arguments: Vec<Object>, line: usize) -> runtime_error::Result<Object>;
    fn arity(&self) -> usize;
}
