mod inner_environment;

use {
    crate::{callable::Clock, object::Object},
    inner_environment::InnerEnvironment,
    std::{cell::RefCell, rc::Rc},
};

#[derive(Clone, Debug)]
pub struct Environment(Rc<RefCell<InnerEnvironment>>);

impl Environment {
    pub fn new_global() -> Self {
        let mut env = Self(Rc::new(RefCell::new(InnerEnvironment::new_global())));

        env.define(String::from("clock"), Object::Callable(Rc::new(Clock)));

        env
    }

    pub fn new_enclosed(enclosing: Environment) -> Self {
        Self(Rc::new(RefCell::new(InnerEnvironment::new(enclosing))))
    }

    pub fn get(&self, name: &str) -> Option<Object> {
        self.0.borrow().get(name)
    }

    pub fn get_at(&self, name: &str, distance: usize) -> Option<Object> {
        self.0.borrow().get_at(name, distance)
    }

    pub fn define(&mut self, name: String, value: Object) {
        self.0.borrow_mut().define(name, value)
    }

    pub fn assign(&mut self, name: String, value: Object) -> bool {
        self.0.borrow_mut().assign(name, value)
    }

    pub fn assign_at(&mut self, name: String, value: Object, distance: usize) -> bool {
        self.0.borrow_mut().assign_at(name, value, distance)
    }
}
