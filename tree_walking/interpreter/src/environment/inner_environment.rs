use {super::Environment, crate::object::Object, std::collections::HashMap};

#[derive(Debug)]
pub struct InnerEnvironment {
    values: HashMap<String, Object>,
    enclosing: Option<Environment>,
}

impl InnerEnvironment {
    pub fn new_global() -> Self {
        Self {
            values: HashMap::new(),
            enclosing: None,
        }
    }

    pub fn new(enclosing: Environment) -> Self {
        Self {
            values: HashMap::new(),
            enclosing: Some(enclosing),
        }
    }

    pub fn get(&self, name: &str) -> Option<Object> {
        self.values.get(name).cloned()
    }

    pub fn get_at(&self, name: &str, distance: usize) -> Option<Object> {
        if distance == 0 {
            self.values.get(name).cloned()
        } else {
            self.enclosing
                .as_ref()
                .and_then(|env| env.get_at(name, distance - 1))
        }
    }

    pub fn define(&mut self, name: String, value: Object) {
        self.values.insert(name, value);
    }

    pub fn assign(&mut self, name: String, value: Object) -> bool {
        match self.values.get_mut(&name) {
            Some(existing) => {
                *existing = value;

                true
            }
            None => false,
        }
    }

    pub fn assign_at(&mut self, name: String, value: Object, distance: usize) -> bool {
        if distance == 0 {
            match self.values.get_mut(&name) {
                Some(existing) => {
                    *existing = value;

                    true
                }
                None => false,
            }
        } else {
            self.enclosing
                .as_mut()
                .map(|env| env.assign_at(name, value, distance - 1))
                .unwrap_or(false)
        }
    }
}
