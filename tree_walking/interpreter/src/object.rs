use {
    crate::{
        callable::{Callable, Class},
        instance::Instance,
    },
    std::{
        fmt::{self, Debug},
        rc::Rc,
    },
};

#[derive(Clone, Debug)]
pub enum Object {
    Nil,
    Boolean(bool),
    Number(f64),
    String(String),
    Callable(Rc<dyn Callable>),
    Class(Class),
    Instance(Instance),
}

impl Object {
    pub fn is_truthy(&self) -> Self {
        match self {
            Object::Nil => Object::Boolean(false),
            Object::Boolean(b) => Object::Boolean(*b),
            _ => Object::Boolean(true),
        }
    }

    pub fn not(&self) -> Self {
        match self {
            Object::Nil => Object::Boolean(true),
            Object::Boolean(b) => Object::Boolean(!*b),
            _ => Object::Boolean(false),
        }
    }
}

impl PartialEq for Object {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Nil, Self::Nil) => true,
            (Self::Boolean(this), Self::Boolean(other)) => this == other,
            (Self::Number(this), Self::Number(other)) => this == other,
            (Self::String(this), Self::String(other)) => this == other,
            (Self::Callable(this), Self::Callable(other)) => this.to_string() == other.to_string(),
            (Self::Class(this), Self::Class(other)) => this == other,
            (Self::Instance(this), Self::Instance(other)) => this == other,
            (_, _) => false,
        }
    }
}

impl fmt::Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Nil => write!(f, "nil"),
            Self::Boolean(true) => write!(f, "true"),
            Self::Boolean(false) => write!(f, "false"),
            Self::Number(n) => write!(f, "{}", n),
            Self::String(s) => write!(f, "\"{}\"", s),
            Self::Callable(c) => write!(f, "{}", c),
            Self::Class(c) => write!(f, "{}", c),
            Self::Instance(i) => write!(f, "{}", i),
        }
    }
}
