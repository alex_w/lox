use {
    super::{Callable, Function},
    crate::{instance::Instance, object::Object, runtime_error},
    std::{collections::HashMap, fmt, rc::Rc},
};

#[derive(Clone, Debug)]
pub struct Class {
    inner: Rc<ClassInner>,
}

#[derive(Debug)]
struct ClassInner {
    name: String,
    superclass: Option<Class>,
    methods: HashMap<String, Function>,
}

impl Class {
    pub fn new(
        name: String,
        superclass: Option<Class>,
        methods: HashMap<String, Function>,
    ) -> Self {
        Self {
            inner: Rc::new(ClassInner {
                name,
                superclass,
                methods,
            }),
        }
    }

    pub fn name(&self) -> &str {
        &self.inner.name
    }

    pub fn get_method(&self, name: &str) -> Option<&Function> {
        self.inner.methods.get(name).or_else(|| {
            self.inner
                .superclass
                .as_ref()
                .and_then(|superclass| superclass.get_method(name))
        })
    }
}

impl fmt::Display for Class {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<class:{}>", self.inner.name)
    }
}

impl Callable for Class {
    fn call(&self, arguments: Vec<Object>, line: usize) -> runtime_error::Result<Object> {
        let instance = Instance::new(self.clone());

        let initializer = self
            .inner
            .methods
            .get("init")
            .map(|init| init.bind(instance.clone()));
        if let Some(initializer) = initializer {
            initializer.call(arguments, line)?;
        }

        Ok(Object::Instance(instance))
    }

    fn arity(&self) -> usize {
        self.inner
            .methods
            .get("init")
            .map(|init| init.arity())
            .unwrap_or(0)
    }
}

impl PartialEq for Class {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.inner, &other.inner)
    }
}
