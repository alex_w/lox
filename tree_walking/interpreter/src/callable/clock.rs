use {
    super::Callable,
    crate::{
        object::Object,
        runtime_error::{self, RuntimeError},
    },
    std::{fmt, time::SystemTime},
};

#[derive(Debug)]
pub struct Clock;

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<native-fn:clock>")
    }
}

impl Callable for Clock {
    fn arity(&self) -> usize {
        0
    }

    fn call(&self, _arguments: Vec<Object>, line: usize) -> runtime_error::Result<Object> {
        match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
            Ok(n) => Ok(Object::Number(n.as_secs_f64())),
            Err(_) => Err(RuntimeError::new("SystemTime before UNIX EPOCH!", line)),
        }
    }
}
