use {
    super::Callable,
    crate::{
        environment::Environment,
        instance::Instance,
        object::Object,
        runtime_error::{self, RuntimeError},
        Interpreter,
    },
    parser::{node_id::NodeId, stmt::Stmt},
    std::{collections::HashMap, fmt, rc::Rc},
};

#[derive(Debug)]
pub struct Function {
    name: String,
    params: Vec<String>,
    body: Vec<Stmt>,
    is_initializer: bool,
    environment: Environment,
    globals: Environment,
    resolutions: Rc<HashMap<NodeId, usize>>,
}

impl Function {
    pub fn new(
        name: String,
        params: Vec<String>,
        body: Vec<Stmt>,
        is_initializer: bool,
        environment: Environment,
        globals: Environment,
        resolutions: Rc<HashMap<NodeId, usize>>,
    ) -> Self {
        Self {
            name,
            params,
            body,
            is_initializer,
            environment,
            globals,
            resolutions,
        }
    }

    pub fn bind(&self, instance: Instance) -> Self {
        let mut environment = Environment::new_enclosed(self.environment.clone());
        environment.define(String::from("this"), Object::Instance(instance));

        Self {
            name: self.name.clone(),
            params: self.params.clone(),
            body: self.body.clone(),
            is_initializer: self.is_initializer,
            environment,
            globals: self.globals.clone(),
            resolutions: self.resolutions.clone(),
        }
    }

    fn this(&self, line: usize) -> runtime_error::Result<Object> {
        self.environment
            .get_at("this", 0)
            .ok_or_else(|| RuntimeError::new("Expected to find 'this' in initializer.", line))
    }
}

impl fmt::Display for Function {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<fun:{}\\{}>", self.name, self.params.len())
    }
}

impl Callable for Function {
    fn call(&self, arguments: Vec<Object>, line: usize) -> runtime_error::Result<Object> {
        let mut environment = Environment::new_enclosed(self.environment.clone());
        for (name, arg) in self.params.iter().cloned().zip(arguments.into_iter()) {
            environment.define(name, arg);
        }
        let mut interpreter = Interpreter::new(environment, self.globals.clone());

        for stmt in self.body.clone() {
            if let Some(result) = interpreter.execute(stmt, self.resolutions.clone())? {
                return if self.is_initializer {
                    self.this(line)
                } else {
                    Ok(result)
                };
            }
        }

        if self.is_initializer {
            self.this(line)
        } else {
            Ok(Object::Nil)
        }
    }

    fn arity(&self) -> usize {
        self.params.len()
    }
}
