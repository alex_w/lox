#[derive(Clone, Debug, PartialEq)]
pub struct Token {
    pub kind: TokenKind,
    /// Line number where the error occured
    pub line: usize,
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind {
    // Single-character tokens.
    /// "("
    LeftParen,
    /// ")"
    RightParen,
    /// "{"
    LeftBrace,
    /// "}"
    RightBrace,
    /// ","
    Comma,
    /// "."
    Dot,
    /// "-"
    Minus,
    /// "+"
    Plus,
    /// ";"
    SemiColon,
    /// "/"
    Slash,
    /// "*"
    Star,

    // One or Two character tokens.
    /// "!"
    Bang,
    /// "!="
    BangEqual,
    /// "="
    Equal,
    /// "=="
    EqualEqual,
    /// ">"
    Greater,
    /// ">="
    GreaterEqual,
    /// "<"
    Less,
    /// "<="
    LessEqual,

    // Literals.
    /// "foo"
    Identifier(String),
    /// ""abc""
    String(String),
    /// "42", "42.0"
    Number(f64),

    // Keywords.
    /// "and"
    And,
    /// "class"
    Class,
    /// "else"
    Else,
    /// "false"
    False,
    /// "fun"
    Fun,
    /// "for"
    For,
    /// "if"
    If,
    /// "nil"
    Nil,
    /// "or"
    Or,
    /// "print"
    Print,
    /// "return"
    Return,
    /// "super"
    Super,
    /// "this"
    This,
    /// "true"
    True,
    /// "var"
    Var,
    /// "while"
    While,

    // Other.
    /// End of file
    Eof,
    /// Unexpected
    Unexpected { cause: String, literal: String },
}
