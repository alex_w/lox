use std::str::Chars;

pub struct Cursor<'a> {
    source: Chars<'a>,
    pub line: usize,
}

impl<'a> Cursor<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source: source.chars(),
            line: 1,
        }
    }

    pub fn advance(&mut self) -> Option<char> {
        let c = self.source.next()?;

        if c == '\n' {
            self.line += 1;
        }

        Some(c)
    }

    fn nth_char(&self, n: usize) -> Option<char> {
        self.source.clone().nth(n)
    }

    pub fn peek(&self) -> Option<char> {
        self.nth_char(0)
    }

    pub fn peek_next(&self) -> Option<char> {
        self.nth_char(1)
    }

    pub fn matches(&mut self, c: char) -> bool {
        let matches = self.peek().map(|next| next == c).unwrap_or(false);

        if matches {
            self.advance();
        }

        matches
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_source_creates_source() {
        let string = "source";

        let _source = Cursor::new(string);
    }

    #[test]
    fn new_source_start_at_line_1() {
        let string = "source";
        let source = Cursor::new(string);

        let line = source.line;

        assert_eq!(1, line);
    }

    #[test]
    fn advance_returns_next_char() {
        let string = "source";
        let mut source = Cursor::new(string);

        let c = source.advance();

        assert_eq!(Some('s'), c);
    }

    #[test]
    fn advance_returns_none_when_empty() {
        let string = "";
        let mut source = Cursor::new(string);

        let c = source.advance();

        assert_eq!(None, c);
    }

    #[test]
    fn advance_advances_line_when_move_over_newline() {
        let string = "\nsource";
        let mut source = Cursor::new(string);
        source.advance();

        let line = source.line;

        assert_eq!(2, line);
    }

    #[test]
    fn advance_does_not_advance_line_when_move_over_other_character() {
        let string = "source";
        let mut source = Cursor::new(string);
        source.advance();

        let line = source.line;

        assert_eq!(1, line);
    }

    #[test]
    fn advance_does_not_advance_line_when_empty() {
        let string = "";
        let mut source = Cursor::new(string);
        source.advance();

        let line = source.line;

        assert_eq!(1, line);
    }

    #[test]
    fn peek_returns_next_char() {
        let string = "source";
        let source = Cursor::new(string);

        let c = source.peek();

        assert_eq!(Some('s'), c);
    }

    #[test]
    fn peek_returns_none_when_empty() {
        let string = "";
        let source = Cursor::new(string);

        let c = source.peek();

        assert_eq!(None, c);
    }

    #[test]
    fn peek_does_not_advance_line_when_peek_newline() {
        let string = "\nsource";
        let source = Cursor::new(string);
        source.peek();

        let line = source.line;

        assert_eq!(1, line);
    }

    #[test]
    fn peek_next_returns_second_char() {
        let string = "source";
        let source = Cursor::new(string);

        let c = source.peek_next();

        assert_eq!(Some('o'), c);
    }

    #[test]
    fn peek_next_returns_none_when_empty() {
        let string = "a";
        let source = Cursor::new(string);

        let c = source.peek_next();

        assert_eq!(None, c);
    }

    #[test]
    fn peek_next_does_not_advance_line_when_peek_newline() {
        let string = "\n\nsource";
        let source = Cursor::new(string);
        source.peek_next();

        let line = source.line;

        assert_eq!(1, line);
    }

    #[test]
    fn matches_returns_false_when_does_not_match() {
        let string = "a";
        let mut source = Cursor::new(string);

        let matches = source.matches('b');

        assert!(!matches);
    }

    #[test]
    fn matches_does_not_consume_character_when_does_not_match() {
        let string = "a";
        let mut source = Cursor::new(string);

        source.matches('b');

        let c = source.peek();
        assert_eq!(Some('a'), c);
    }

    #[test]
    fn matches_returns_false_when_empty_input() {
        let string = "";
        let mut source = Cursor::new(string);

        let matches = source.matches('b');

        assert!(!matches);
    }

    #[test]
    fn matches_returns_true_when_matches() {
        let string = "a";
        let mut source = Cursor::new(string);

        let matches = source.matches('a');

        assert!(matches);
    }

    #[test]
    fn matches_consumes_character_when_matches() {
        let string = "ab";
        let mut source = Cursor::new(string);

        source.matches('a');

        let c = source.peek();
        assert_eq!(Some('b'), c);
    }
}
