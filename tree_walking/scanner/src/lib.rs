mod cursor;
pub mod token;

use {
    cursor::Cursor,
    token::{Token, TokenKind},
};

pub struct Scanner<'a> {
    source: Cursor<'a>,
    finished: bool,
}

impl<'a> Scanner<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source: Cursor::new(source),
            finished: false,
        }
    }

    fn next_token(&mut self) -> Option<Token> {
        let line = self.source.line;

        let kind = if let Some(c) = self.source.advance() {
            match c {
                // Whitespace.
                ' ' | '\t' | '\r' | '\n' => return None,

                // One character tokens.
                '(' => TokenKind::LeftParen,
                ')' => TokenKind::RightParen,
                '{' => TokenKind::LeftBrace,
                '}' => TokenKind::RightBrace,
                ',' => TokenKind::Comma,
                '.' => TokenKind::Dot,
                '-' => TokenKind::Minus,
                '+' => TokenKind::Plus,
                ';' => TokenKind::SemiColon,
                '*' => TokenKind::Star,

                // One or two character tokens.
                '!' if self.source.matches('=') => TokenKind::BangEqual,
                '!' => TokenKind::Bang,
                '=' if self.source.matches('=') => TokenKind::EqualEqual,
                '=' => TokenKind::Equal,
                '>' if self.source.matches('=') => TokenKind::GreaterEqual,
                '>' => TokenKind::Greater,
                '<' if self.source.matches('=') => TokenKind::LessEqual,
                '<' => TokenKind::Less,

                // Literal.
                '"' => self.string(),
                d if is_digit(d) => self.number(d),
                c if is_alpha(c) => self.identifier(c),

                // Other.
                '/' if self.source.matches('/') => {
                    self.comment();

                    return None;
                }
                '/' => TokenKind::Slash,

                c => TokenKind::Unexpected {
                    cause: String::from("Unknown character"),
                    literal: c.to_string(),
                },
            }
        } else {
            self.finished = true;
            TokenKind::Eof
        };

        Some(Token { kind, line })
    }

    fn string(&mut self) -> TokenKind {
        let mut string = String::new();
        while let Some(c) = self.source.advance() {
            if c == '"' {
                return TokenKind::String(string);
            } else {
                string.push(c);
            }
        }

        TokenKind::Unexpected {
            cause: String::from("Expected end of string"),
            literal: string,
        }
    }

    fn number(&mut self, c: char) -> TokenKind {
        let mut string = String::from(c);
        while let Some(c) = self.source.peek() {
            if is_digit(c) {
                string.push(c);
                self.source.advance();
            } else {
                break;
            }
        }

        if let (Some(p), Some(pn)) = (self.source.peek(), self.source.peek_next()) {
            if p == '.' && is_digit(pn) {
                string.push(p);
                self.source.advance();

                while let Some(c) = self.source.peek() {
                    if is_digit(c) {
                        string.push(c);
                        self.source.advance();
                    } else {
                        break;
                    }
                }
            }
        }

        if let Ok(n) = string.parse() {
            TokenKind::Number(n)
        } else {
            TokenKind::Unexpected {
                cause: String::from("Failed to parse number"),
                literal: string,
            }
        }
    }

    fn identifier(&mut self, c: char) -> TokenKind {
        let mut identifier = String::from(c);
        while let Some(c) = self.source.peek() {
            if is_alphanumeric(c) {
                identifier.push(c);
                self.source.advance();
            } else {
                break;
            }
        }

        match get_keyword(&identifier) {
            Some(keyword) => keyword,
            None => TokenKind::Identifier(identifier),
        }
    }

    fn comment(&mut self) {
        loop {
            match self.source.advance() {
                None | Some('\n') => break,
                _ => {}
            }
        }
    }
}

fn is_digit(c: char) -> bool {
    matches!(c, '0'..='9')
}

fn is_alpha(c: char) -> bool {
    matches!(c, 'a'..='z' | 'A'..='Z' | '_')
}

fn is_alphanumeric(c: char) -> bool {
    is_digit(c) | is_alpha(c)
}

fn get_keyword(word: &str) -> Option<TokenKind> {
    match word {
        "and" => Some(TokenKind::And),
        "class" => Some(TokenKind::Class),
        "else" => Some(TokenKind::Else),
        "false" => Some(TokenKind::False),
        "for" => Some(TokenKind::For),
        "fun" => Some(TokenKind::Fun),
        "if" => Some(TokenKind::If),
        "nil" => Some(TokenKind::Nil),
        "or" => Some(TokenKind::Or),
        "print" => Some(TokenKind::Print),
        "return" => Some(TokenKind::Return),
        "super" => Some(TokenKind::Super),
        "this" => Some(TokenKind::This),
        "true" => Some(TokenKind::True),
        "var" => Some(TokenKind::Var),
        "while" => Some(TokenKind::While),
        _ => None,
    }
}

impl<'a> Iterator for Scanner<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            None
        } else {
            loop {
                let token = self.next_token();
                if token.is_some() {
                    return token;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use token::TokenKind;

    use super::*;

    #[test]
    fn new_scanner() {
        let source = "source";

        let _scanner = Scanner::new(source);
    }

    #[test]
    fn next_single_character_tokens() {
        let source = "(){},.-+;/*";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::LeftParen,
                TokenKind::RightParen,
                TokenKind::LeftBrace,
                TokenKind::RightBrace,
                TokenKind::Comma,
                TokenKind::Dot,
                TokenKind::Minus,
                TokenKind::Plus,
                TokenKind::SemiColon,
                TokenKind::Slash,
                TokenKind::Star,
                TokenKind::Eof,
            ]
        );
    }

    #[test]
    fn next_one_or_two_character_tokens() {
        let source = "!!====>>=<<=";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Bang,
                TokenKind::BangEqual,
                TokenKind::EqualEqual,
                TokenKind::Equal,
                TokenKind::Greater,
                TokenKind::GreaterEqual,
                TokenKind::Less,
                TokenKind::LessEqual,
                TokenKind::Eof,
            ]
        );
    }

    #[test]
    fn next_single_line_comments() {
        let source = "// a comment\n==// another comment";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(tokens, vec![TokenKind::EqualEqual, TokenKind::Eof]);
    }

    #[test]
    fn next_ignores_whitespace() {
        let source = "= ==\t=\r==\n=";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Equal,
                TokenKind::EqualEqual,
                TokenKind::Equal,
                TokenKind::EqualEqual,
                TokenKind::Equal,
                TokenKind::Eof
            ]
        );
    }

    #[test]
    fn next_string() {
        let string = "a \nstring";
        let source = format!(r#""{}""#, string);
        let scanner = Scanner::new(&source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![TokenKind::String(String::from(string)), TokenKind::Eof]
        );
    }

    #[test]
    fn next_unclosed_string() {
        let string = "unclosed string";
        let source = format!("\n\"{}", string);
        let scanner = Scanner::new(&source);

        let tokens = scanner.collect::<Vec<Token>>();

        assert_eq!(
            tokens,
            vec![
                Token {
                    kind: TokenKind::Unexpected {
                        cause: String::from("Expected end of string"),
                        literal: String::from(string),
                    },
                    line: 2,
                },
                Token {
                    kind: TokenKind::Eof,
                    line: 2,
                },
            ]
        );
    }

    #[test]
    fn next_digit() {
        let source = "1 2 3 4.2";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Number(1.0),
                TokenKind::Number(2.0),
                TokenKind::Number(3.0),
                TokenKind::Number(4.2),
                TokenKind::Eof,
            ]
        );
    }

    #[test]
    fn next_identifier() {
        let source = "foo bar baz xyzzy";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Identifier(String::from("foo")),
                TokenKind::Identifier(String::from("bar")),
                TokenKind::Identifier(String::from("baz")),
                TokenKind::Identifier(String::from("xyzzy")),
                TokenKind::Eof,
            ]
        );
    }

    #[test]
    fn next_keywords() {
        let source =
            "and class else false for fun if nil or print return super this true var while";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::And,
                TokenKind::Class,
                TokenKind::Else,
                TokenKind::False,
                TokenKind::For,
                TokenKind::Fun,
                TokenKind::If,
                TokenKind::Nil,
                TokenKind::Or,
                TokenKind::Print,
                TokenKind::Return,
                TokenKind::Super,
                TokenKind::This,
                TokenKind::True,
                TokenKind::Var,
                TokenKind::While,
                TokenKind::Eof,
            ]
        );
    }

    #[test]
    fn next_line_numbers() {
        let source = r#"!// line 1
        !// line 2
        !// line 3
        !// line 4
        "   line 5

        "// line 7"#;
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|token| token.line).collect::<Vec<usize>>();

        assert_eq!(tokens, vec![1, 2, 3, 4, 5, 7,]);
    }

    #[test]
    fn next_unknown_character() {
        let unknown_character = "@";
        let source = format!("\n{}", unknown_character);
        let scanner = Scanner::new(&source);

        let tokens = scanner.collect::<Vec<Token>>();

        assert_eq!(
            tokens,
            vec![
                Token {
                    kind: TokenKind::Unexpected {
                        cause: String::from("Unknown character"),
                        literal: String::from(unknown_character),
                    },
                    line: 2,
                },
                Token {
                    kind: TokenKind::Eof,
                    line: 2,
                },
            ]
        );
    }
}
