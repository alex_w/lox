use std::{
    env,
    error::Error,
    io::Write,
    process::{Command, Stdio},
};

const BIN: &str = env!("CARGO_BIN_EXE_tlox");
const LOX_FILE: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/tests_lox/hello_world.lx");
const NO_FILE: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/tests_lox/no_file.lx");
const INVALID_FILE: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/tests_lox/invalid.lx");

#[test]
fn two_arguments_error() -> Result<(), Box<dyn Error>> {
    let status = Command::new(BIN)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .arg("first")
        .arg("second")
        .status()?;

    assert!(!status.success());

    Ok(())
}

#[test]
fn one_argument_valid_file() -> Result<(), Box<dyn Error>> {
    let status = Command::new(BIN)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .arg(LOX_FILE)
        .status()?;

    assert!(status.success());

    Ok(())
}

#[test]
fn one_argument_invalid_lox_file() -> Result<(), Box<dyn Error>> {
    let status = Command::new(BIN)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .arg(INVALID_FILE)
        .status()?;

    assert!(!status.success());

    Ok(())
}

#[test]
fn one_argument_invalid_path() -> Result<(), Box<dyn Error>> {
    let status = Command::new(BIN)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .arg(NO_FILE)
        .status()?;

    assert!(!status.success());

    Ok(())
}

#[test]
fn no_arguments() -> Result<(), Box<dyn Error>> {
    let mut child = Command::new(BIN)
        .stdin(Stdio::piped())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .spawn()?;

    child.stdin.take().unwrap().write_all(b"")?;

    let status = child.wait()?;

    assert!(status.success());

    Ok(())
}
