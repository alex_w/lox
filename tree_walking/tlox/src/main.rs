use {
    interpreter::Interpreter,
    parser::{Parser, Resolver},
    scanner::Scanner,
    std::{
        env,
        error::Error,
        fs,
        io::{self, BufRead, Write},
    },
};

fn main() -> Result<(), String> {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        0 | 1 => {
            let stdin = io::stdin();
            let reader = stdin.lock();
            let writer = io::stdout();

            repl(reader, writer)
        }
        2 => run_file(&args[1]),
        _ => Err(String::from("Expect 0 or 1 arguments")),
    }
}

fn run_file(path: &str) -> Result<(), String> {
    let file = fs::read_to_string(path).map_err(|_| String::from("Error reading file"))?;
    let mut interpreter = Interpreter::default();

    match run(&file, &mut interpreter) {
        Ok(()) => Ok(()),
        Err(err) => Err(err.to_string()),
    }
}

fn repl<R, W>(mut reader: R, mut writer: W) -> Result<(), String>
where
    R: BufRead,
    W: Write,
{
    let mut interpreter = Interpreter::default();

    writeln!(&mut writer, "Lox repl:").map_err(|_| String::from("Failed to write output"))?;
    loop {
        write!(&mut writer, "❯ ").map_err(|_| String::from("Failed to write output"))?;
        writer
            .flush()
            .map_err(|_| String::from("Failed to write to output"))?;

        match read_line(&mut reader)? {
            None => break,
            Some(line) => {
                if let Err(err) = run(&line, &mut interpreter) {
                    println!("Error(s):\n{}", err)
                }
            }
        }
    }

    Ok(())
}

fn read_line<R>(reader: &mut R) -> Result<Option<String>, String>
where
    R: BufRead,
{
    let mut line = String::new();
    let bytes_read = reader
        .read_line(&mut line)
        .map_err(|_| String::from("Failed to read input"))?;

    match bytes_read {
        0 => Ok(None),
        _ => Ok(Some(line)),
    }
}

fn run(source: &str, interpreter: &mut Interpreter) -> Result<(), Box<dyn Error>> {
    let scanner = Scanner::new(source);

    let mut parser = Parser::new(scanner);

    let stmts = parser.parse()?;

    let resolver = Resolver::default();
    let resolutions = resolver.resolve(&stmts)?;

    Ok(interpreter.interpret(stmts, resolutions)?)
}

#[cfg(test)]
mod tests {
    use {super::*, std::error::Error};

    const LOX_FILE: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/tests_lox/hello_world.lx");
    const NO_FILE: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/tests_lox/no_file.lx");

    #[test]
    fn run_file_valid_file_ok() {
        let arg = LOX_FILE;

        let result = run_file(arg);

        assert_eq!(Ok(()), result);
    }

    #[test]
    fn run_file_no_file_at_path_err() {
        let arg = NO_FILE;

        let result = run_file(arg);

        assert!(matches!(result, Err(_)));
    }

    #[test]
    fn repl_no_input() -> Result<(), Box<dyn Error>> {
        let input = b"";
        let mut output = Vec::new();

        repl(&input[..], &mut output)?;

        let output = String::from_utf8(output)?;
        assert_eq!(String::from("Lox repl:\n❯ "), output);

        Ok(())
    }

    #[test]
    fn repl_one_line() -> Result<(), Box<dyn Error>> {
        let input = b"\n";
        let mut output = Vec::new();

        repl(&input[..], &mut output)?;

        let output = String::from_utf8(output)?;
        assert_eq!(String::from("Lox repl:\n❯ ❯ "), output);

        Ok(())
    }

    #[test]
    fn read_line_valid_input() -> Result<(), Box<dyn Error>> {
        let input = "valid input";

        let line = read_line(&mut input.as_bytes())?;

        assert_eq!(Some(String::from(input)), line);

        Ok(())
    }

    #[test]
    fn read_line_invalid_input() {
        let input = [34u8, 228, 166, 164, 110, 237, 166, 164, 44, 34];

        let line = read_line(&mut &input[..]);

        assert_eq!(Err(String::from("Failed to read input")), line);
    }
}
