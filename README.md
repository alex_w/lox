# Lox

An implementation of the Lox language described by the [Crafting Interpreters book by Robert Nystrom](http://craftinginterpreters.com/contents.html)
