use crate::value::{Value, ValuePosition, Values};

pub enum OpCode {
    Constant(ValuePosition),
    Add,
    Subtract,
    Multiply,
    Divide,
    Negate,
    Return,
}

impl OpCode {
    pub fn disassemble(&self, constants: &Values) -> String {
        match self {
            OpCode::Constant(value_position) => {
                let value = constants.get(value_position);

                let name = "OP_CONSTANT";
                format!("{name:16} {value_position:04?} {value:?}")
            }
            OpCode::Add => {
                let name = "OP_ADD";
                format!("{name:16}")
            }
            OpCode::Subtract => {
                let name = "OP_SUBTRACT";
                format!("{name:16}")
            }
            OpCode::Multiply => {
                let name = "OP_MULTIPLY";
                format!("{name:16}")
            }
            OpCode::Divide => {
                let name = "OP_DIVIDE";
                format!("{name:16}")
            }
            OpCode::Negate => {
                let name = "OP_NEGATE";
                format!("{name:16}")
            }
            OpCode::Return => {
                let name = "OP_RETURN";
                format!("{name:16}")
            }
        }
    }
}

#[derive(Default)]
pub struct Chunk {
    pub ops: Vec<OpCode>,
    lines: Vec<usize>,
    pub constants: Values,
}

impl Chunk {
    pub fn write(&mut self, op: OpCode, line: usize) {
        self.ops.push(op);
        self.lines.push(line);
    }

    pub fn add_constant(&mut self, value: Value) -> ValuePosition {
        self.constants.write(value)
    }

    pub fn disassemble(&self, name: &str) {
        println!("== {name} ==");
        let mut previous = 0;
        for (i, (op, line)) in self.ops.iter().zip(self.lines.iter()).enumerate() {
            let op = op.disassemble(&self.constants);

            let line_text = if *line == previous {
                String::from("   |")
            } else {
                format!("{line:04}")
            };
            previous = *line;

            println!("{i:04} {line_text} {op}");
        }
    }
}
