mod cursor;
pub mod token;

pub use token::{Token, TokenKind};

use self::cursor::Cursor;

pub struct Scanner<'a> {
    source: Cursor<'a>,
    line: usize,
}

impl<'a> Scanner<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source: Cursor::new(source),
            line: 1,
        }
    }

    fn eat_whitespace(&mut self) {
        while let Some(c) = self.source.peek() {
            match c {
                ' ' | '\t' | '\r' => {
                    self.source.advance();
                }
                '\n' => {
                    self.source.advance();
                    self.line += 1;
                }
                '/' if self.source.peek_next() == Some('/') => {
                    while let Some(c) = self.source.peek() {
                        if matches!(c, '\n') {
                            break;
                        } else {
                            self.source.advance();
                        }
                    }
                }
                _ => break,
            }
        }
    }

    fn eat_digits(&mut self) {
        while let Some(c) = self.source.peek() {
            if is_digit(c) {
                self.source.advance();
            } else {
                break;
            }
        }
    }

    fn eat_alpha_or_digit(&mut self) {
        while let Some(c) = self.source.peek() {
            if is_alpha(c) || is_digit(c) {
                self.source.advance();
            } else {
                break;
            }
        }
    }

    fn string(&mut self) -> TokenKind {
        let mut terminated = false;
        while let Some(c) = self.source.peek() {
            match c {
                '\n' => self.line += 1,
                '"' => terminated = true,
                _ => {}
            }
            self.source.advance();
        }

        if terminated {
            TokenKind::String
        } else {
            TokenKind::Unexpected {
                cause: String::from("Expected end of string."),
            }
        }
    }

    fn number(&mut self) -> TokenKind {
        self.eat_digits();
        if let (Some(c), Some(cn)) = (self.source.peek(), self.source.peek_next()) {
            if c == '.' && is_digit(cn) {
                self.source.advance();
                self.eat_digits();
            }
        }

        TokenKind::Number
    }

    fn check_keyword(
        &mut self,
        expected: impl IntoIterator<Item = char>,
        kind: TokenKind,
    ) -> TokenKind {
        for expected in expected {
            match self.source.peek() {
                Some(actual) if actual == expected => {
                    self.source.advance();
                }
                Some(c) if is_alpha(c) || is_digit(c) => {
                    self.source.advance();
                    self.eat_alpha_or_digit();
                    return TokenKind::Identifier;
                }
                _ => return TokenKind::Identifier,
            }
        }

        kind
    }

    fn identifier(&mut self, c: char) -> TokenKind {
        match c {
            'a' => self.check_keyword(['n', 'd'], TokenKind::And),
            'c' => self.check_keyword(['l', 'a', 's', 's'], TokenKind::Class),
            'e' => self.check_keyword(['l', 's', 'e'], TokenKind::Else),
            'f' => {
                let next = self.source.peek();
                match next {
                    Some('a') => {
                        self.source.advance();
                        self.check_keyword(['l', 's', 'e'], TokenKind::False)
                    }
                    Some('o') => {
                        self.source.advance();
                        self.check_keyword(['r'], TokenKind::For)
                    }
                    Some('u') => {
                        self.source.advance();
                        self.check_keyword(['n'], TokenKind::Fun)
                    }
                    Some(c) if is_alpha(c) || is_digit(c) => {
                        self.source.advance();
                        self.eat_alpha_or_digit();
                        TokenKind::Identifier
                    }
                    _ => TokenKind::Identifier,
                }
            }
            'i' => self.check_keyword(['f'], TokenKind::If),
            'n' => self.check_keyword(['i', 'l'], TokenKind::Nil),
            'o' => self.check_keyword(['r'], TokenKind::Or),
            'p' => self.check_keyword(['r', 'i', 'n', 't'], TokenKind::Print),
            'r' => self.check_keyword(['e', 't', 'u', 'r', 'n'], TokenKind::Return),
            's' => self.check_keyword(['u', 'p', 'e', 'r'], TokenKind::Super),
            't' => {
                let next = self.source.peek();
                match next {
                    Some('h') => {
                        self.source.advance();
                        self.check_keyword(['i', 's'], TokenKind::This)
                    }
                    Some('r') => {
                        self.source.advance();
                        self.check_keyword(['u', 'e'], TokenKind::True)
                    }
                    Some(c) if is_alpha(c) || is_digit(c) => {
                        self.source.advance();
                        self.eat_alpha_or_digit();
                        TokenKind::Identifier
                    }
                    _ => TokenKind::Identifier,
                }
            }
            'v' => self.check_keyword(['a', 'r'], TokenKind::Var),
            'w' => self.check_keyword(['h', 'i', 'l', 'e'], TokenKind::While),
            c if is_alpha(c) || is_digit(c) => {
                self.source.advance();
                self.eat_alpha_or_digit();
                TokenKind::Identifier
            }
            _ => TokenKind::Identifier,
        }
    }

    fn next_token(&mut self) -> Option<Token> {
        self.eat_whitespace();

        let start = self.source.len_consumed();

        let next = self.source.advance()?;

        let kind = match next {
            // One character tokens.
            '(' => TokenKind::LeftParen,
            ')' => TokenKind::RightParen,
            '{' => TokenKind::LeftBrace,
            '}' => TokenKind::RightBrace,
            ';' => TokenKind::SemiColon,
            ',' => TokenKind::Comma,
            '.' => TokenKind::Dot,
            '-' => TokenKind::Minus,
            '+' => TokenKind::Plus,
            '/' => TokenKind::Slash,
            '*' => TokenKind::Star,

            // One or two character tokens.
            '!' if self.source.matches('=') => TokenKind::BangEqual,
            '!' => TokenKind::Bang,
            '=' if self.source.matches('=') => TokenKind::EqualEqual,
            '=' => TokenKind::Equal,
            '<' if self.source.matches('=') => TokenKind::LessEqual,
            '<' => TokenKind::Less,
            '>' if self.source.matches('=') => TokenKind::GreaterEqual,
            '>' => TokenKind::Greater,

            // Literal.
            '"' => self.string(),
            c if is_digit(c) => self.number(),
            c if is_alpha(c) => self.identifier(c),

            // Other.
            c => TokenKind::Unexpected {
                cause: format!("Unexpected character: `{c}`."),
            },
        };

        let end = self.source.len_consumed();

        Some(Token {
            kind,
            start,
            length: end - start,
            line: self.line,
        })
    }
}

impl Iterator for Scanner<'_> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_token()
    }
}

fn is_whitespace(c: char) -> bool {
    matches!(c, ' ' | '\r' | '\t')
}

fn is_digit(c: char) -> bool {
    matches!(c, '0'..='9')
}

fn is_alpha(c: char) -> bool {
    matches!(c, 'a'..='z' | 'A'..='Z' | '_')
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scanner_returns_correct_token_positions() {
        let scanner = Scanner::new("+ foo 42");
        let expected = vec![
            Token {
                kind: TokenKind::Plus,
                start: 0,
                length: 1,
                line: 1,
            },
            Token {
                kind: TokenKind::Identifier,
                start: 2,
                length: 3,
                line: 1,
            },
            Token {
                kind: TokenKind::Number,
                start: 6,
                length: 2,
                line: 1,
            },
        ];

        let tokens: Vec<Token> = scanner.collect();

        assert_eq!(expected, tokens);
    }

    #[test]
    fn next_single_character_tokens() {
        let source = "(){},.-+;/*";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::LeftParen,
                TokenKind::RightParen,
                TokenKind::LeftBrace,
                TokenKind::RightBrace,
                TokenKind::Comma,
                TokenKind::Dot,
                TokenKind::Minus,
                TokenKind::Plus,
                TokenKind::SemiColon,
                TokenKind::Slash,
                TokenKind::Star,
            ]
        );
    }

    #[test]
    fn next_one_or_two_character_tokens() {
        let source = "!!====>>=<<=";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Bang,
                TokenKind::BangEqual,
                TokenKind::EqualEqual,
                TokenKind::Equal,
                TokenKind::Greater,
                TokenKind::GreaterEqual,
                TokenKind::Less,
                TokenKind::LessEqual,
            ]
        );
    }

    #[test]
    fn next_single_line_comments() {
        let source = "// a comment\n==// another comment";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(tokens, vec![TokenKind::EqualEqual]);
    }

    #[test]
    fn next_ignores_whitespace() {
        let source = "= ==\t=\r==\n=";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Equal,
                TokenKind::EqualEqual,
                TokenKind::Equal,
                TokenKind::EqualEqual,
                TokenKind::Equal,
            ]
        );
    }

    #[test]
    fn next_string() {
        let string = "a \nstring";
        let source = format!(r#""{}""#, string);
        let scanner = Scanner::new(&source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(tokens, vec![TokenKind::String]);
    }

    #[test]
    fn next_unclosed_string() {
        let string = "unclosed string";
        let source = format!("\n\"{}", string);
        let scanner = Scanner::new(&source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![TokenKind::Unexpected {
                cause: String::from("Expected end of string."),
            }]
        );
    }

    #[test]
    fn next_digit() {
        let source = "1 2 3 4.2";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Number,
                TokenKind::Number,
                TokenKind::Number,
                TokenKind::Number,
            ]
        );
    }

    #[test]
    fn next_identifier() {
        let source = "foo bar baz xyzzy";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::Identifier,
                TokenKind::Identifier,
                TokenKind::Identifier,
                TokenKind::Identifier,
            ]
        );
    }

    #[test]
    fn next_keywords() {
        let source =
            "and class else false for fun if nil or print return super this true var while";
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![
                TokenKind::And,
                TokenKind::Class,
                TokenKind::Else,
                TokenKind::False,
                TokenKind::For,
                TokenKind::Fun,
                TokenKind::If,
                TokenKind::Nil,
                TokenKind::Or,
                TokenKind::Print,
                TokenKind::Return,
                TokenKind::Super,
                TokenKind::This,
                TokenKind::True,
                TokenKind::Var,
                TokenKind::While,
            ]
        );
    }

    #[test]
    fn next_line_numbers() {
        let source = r#"!// line 1
        !// line 2
        !// line 3
        !// line 4
        "   line 5

        "// line 7"#;
        let scanner = Scanner::new(source);

        let tokens = scanner.map(|token| token.line).collect::<Vec<usize>>();

        assert_eq!(tokens, vec![1, 2, 3, 4, 7]);
    }

    #[test]
    fn next_unknown_character() {
        let unknown_character = "@";
        let source = format!("\n{}", unknown_character);
        let scanner = Scanner::new(&source);

        let tokens = scanner.map(|t| t.kind).collect::<Vec<TokenKind>>();

        assert_eq!(
            tokens,
            vec![TokenKind::Unexpected {
                cause: format!("Unexpected character: `{unknown_character}`."),
            }]
        );
    }
}
