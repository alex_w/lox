use crate::{
    chunk::{Chunk, OpCode},
    compiler::compile,
    value::Value,
};

pub struct Vm {
    stack: Vec<Value>,
}

impl Default for Vm {
    fn default() -> Self {
        Self {
            stack: Vec::with_capacity(256),
        }
    }
}

#[derive(Debug)]
pub enum InterpretError {
    RuntimeError,
}

macro_rules! binary_op {
    ($stack: expr, $op:tt) => {
        let Value(rhs) = $stack.pop().unwrap();
        let Value(lhs) = $stack.pop().unwrap();

        let result = lhs $op rhs;
        $stack.push(Value(result));
    };
}

impl Vm {
    pub fn interpret(&mut self, source: String) -> Result<(), InterpretError> {
        compile(source);

        Ok(())
    }

    pub fn run(&mut self, chunk: &Chunk) -> Result<(), InterpretError> {
        for op in &chunk.ops {
            #[cfg(feature = "trace")]
            self.trace(op, chunk);

            match op {
                OpCode::Constant(value_position) => {
                    let constant = chunk.constants.get(value_position);
                    self.stack.push(constant.clone());
                }
                OpCode::Add => {
                    binary_op!(self.stack, +);
                }
                OpCode::Subtract => {
                    binary_op!(self.stack, -);
                }
                OpCode::Multiply => {
                    binary_op!(self.stack, *);
                }
                OpCode::Divide => {
                    binary_op!(self.stack, /);
                }
                OpCode::Negate => {
                    let Value(value) = self.stack.pop().unwrap();
                    self.stack.push(Value(-value));
                }
                OpCode::Return => {
                    let value = self.stack.pop().unwrap();
                    println!("{value:?}");

                    return Ok(());
                }
            }
        }

        Err(InterpretError::RuntimeError)
    }

    #[cfg(feature = "trace")]
    fn trace(&self, op: &OpCode, chunk: &Chunk) {
        for value in &self.stack {
            print!("[{value:?}]");
        }

        let op = op.disassemble(&chunk.constants);
        println!("\n{op}");
    }
}
