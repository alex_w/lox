use std::fmt::{self, Debug, Formatter};

#[derive(Clone, Default)]
pub struct Value(pub f64);

impl Debug for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let value = self.0;
        write!(f, "{value:?}")
    }
}

#[derive(Default)]
pub struct Values(Vec<Value>);

pub struct ValuePosition(usize);

impl Debug for ValuePosition {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let value = self.0;
        write!(f, "{value:04}")
    }
}

impl Values {
    pub fn write(&mut self, value: Value) -> ValuePosition {
        self.0.push(value);

        ValuePosition(self.0.len() - 1)
    }

    pub fn get(&self, position: &ValuePosition) -> &Value {
        &self.0[position.0]
    }
}
