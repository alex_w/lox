#![allow(dead_code)]

mod chunk;
mod compiler;
mod scanner;
mod value;
mod vm;

use anyhow::anyhow;
use std::{
    env, fs,
    io::{self, BufRead, Write},
};

fn main() -> Result<(), anyhow::Error> {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        0 | 1 => {
            let stdin = io::stdin();
            let mut reader = stdin.lock();
            let mut writer = io::stdout();

            repl(&mut reader, &mut writer)
        }
        2 => run_file(&args[1]),
        _ => Err(anyhow!("Expect 0 or 1 arguments.")),
    }
}

fn repl(reader: &mut impl BufRead, writer: &mut impl Write) -> Result<(), anyhow::Error> {
    writeln!(writer, "Lox repl:")?;
    loop {
        write!(writer, "> ")?;
        writer.flush()?;

        match read_line(reader)? {
            None => break,
            Some(_line) => todo!(),
        }
    }

    Ok(())
}

fn read_line(reader: &mut impl BufRead) -> Result<Option<String>, anyhow::Error> {
    let mut line = String::new();
    let bytes_read = reader.read_line(&mut line)?;

    match bytes_read {
        0 => Ok(None),
        _ => Ok(Some(line)),
    }
}

fn run_file(path: &str) -> Result<(), anyhow::Error> {
    let _file = fs::read_to_string(path)?;

    todo!()
}
