use std::str::Chars;

pub struct Cursor<'a> {
    source: Chars<'a>,
    initial_len: usize,
}

impl<'a> Cursor<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source: source.chars(),
            initial_len: source.len(),
        }
    }

    pub fn advance(&mut self) -> Option<char> {
        self.source.next()
    }

    pub fn peek(&self) -> Option<char> {
        self.source.clone().next()
    }

    pub fn peek_next(&self) -> Option<char> {
        let mut source = self.source.clone();
        source.next();

        source.next()
    }

    pub fn matches(&mut self, c: char) -> bool {
        let matches = self.peek().map(|next| next == c).unwrap_or(false);

        if matches {
            self.advance();
        }

        matches
    }

    pub fn len_consumed(&self) -> usize {
        self.initial_len - self.source.as_str().len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_source_creates_source() {
        let string = "source";

        let _source = Cursor::new(string);
    }

    #[test]
    fn advance_returns_next_char() {
        let string = "source";
        let mut source = Cursor::new(string);

        let c = source.advance();

        assert_eq!(Some('s'), c);
    }

    #[test]
    fn advance_returns_none_when_empty() {
        let string = "";
        let mut source = Cursor::new(string);

        let c = source.advance();

        assert_eq!(None, c);
    }

    #[test]
    fn peek_returns_next_char() {
        let string = "source";
        let source = Cursor::new(string);

        let c = source.peek();

        assert_eq!(Some('s'), c);
    }

    #[test]
    fn peek_returns_none_when_empty() {
        let string = "";
        let source = Cursor::new(string);

        let c = source.peek();

        assert_eq!(None, c);
    }

    #[test]
    fn peek_next_returns_second_char() {
        let string = "source";
        let source = Cursor::new(string);

        let c = source.peek_next();

        assert_eq!(Some('o'), c);
    }

    #[test]
    fn peek_next_returns_none_when_empty() {
        let string = "a";
        let source = Cursor::new(string);

        let c = source.peek_next();

        assert_eq!(None, c);
    }

    #[test]
    fn matches_returns_false_when_does_not_match() {
        let string = "a";
        let mut source = Cursor::new(string);

        let matches = source.matches('b');

        assert!(!matches);
    }

    #[test]
    fn matches_does_not_consume_character_when_does_not_match() {
        let string = "a";
        let mut source = Cursor::new(string);

        source.matches('b');

        let c = source.peek();
        assert_eq!(Some('a'), c);
    }

    #[test]
    fn matches_returns_false_when_empty_input() {
        let string = "";
        let mut source = Cursor::new(string);

        let matches = source.matches('b');

        assert!(!matches);
    }

    #[test]
    fn matches_returns_true_when_matches() {
        let string = "a";
        let mut source = Cursor::new(string);

        let matches = source.matches('a');

        assert!(matches);
    }

    #[test]
    fn matches_consumes_character_when_matches() {
        let string = "ab";
        let mut source = Cursor::new(string);

        source.matches('a');

        let c = source.peek();
        assert_eq!(Some('b'), c);
    }

    #[test]
    fn len_consumed_returns_0_when_not_advanced() {
        let string = "abcdef";
        let source = Cursor::new(string);

        let len_consumed = source.len_consumed();

        assert_eq!(0, len_consumed);
    }

    #[test]
    fn len_consumed_returns_1_when_advanced_once() {
        let string = "abcdef";
        let mut source = Cursor::new(string);
        source.advance();

        let len_consumed = source.len_consumed();

        assert_eq!(1, len_consumed);
    }

    #[test]
    fn len_consumed_returns_0_when_peeked_once() {
        let string = "abcdef";
        let source = Cursor::new(string);
        source.peek();

        let len_consumed = source.len_consumed();

        assert_eq!(0, len_consumed);
    }
}
