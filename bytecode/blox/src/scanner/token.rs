#[derive(Clone, Debug, PartialEq)]
pub struct Token {
    /// Type of the token
    pub kind: TokenKind,
    /// Start index of the token in the source string
    pub start: usize,
    /// Length of the token in the source string
    pub length: usize,
    /// Line number where the error occured
    pub line: usize,
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind {
    // Single-character tokens.
    /// "("
    LeftParen,
    /// ")"
    RightParen,
    /// "{"
    LeftBrace,
    /// "}"
    RightBrace,
    /// ","
    Comma,
    /// "."
    Dot,
    /// "-"
    Minus,
    /// "+"
    Plus,
    /// ";"
    SemiColon,
    /// "/"
    Slash,
    /// "*"
    Star,

    // One or Two character tokens.
    /// "!"
    Bang,
    /// "!="
    BangEqual,
    /// "="
    Equal,
    /// "=="
    EqualEqual,
    /// ">"
    Greater,
    /// ">="
    GreaterEqual,
    /// "<"
    Less,
    /// "<="
    LessEqual,

    // Literals.
    /// "foo"
    Identifier,
    /// ""abc""
    String,
    /// "42", "42.0"
    Number,

    // Keywords.
    /// "and"
    And,
    /// "class"
    Class,
    /// "else"
    Else,
    /// "false"
    False,
    /// "fun"
    Fun,
    /// "for"
    For,
    /// "if"
    If,
    /// "nil"
    Nil,
    /// "or"
    Or,
    /// "print"
    Print,
    /// "return"
    Return,
    /// "super"
    Super,
    /// "this"
    This,
    /// "true"
    True,
    /// "var"
    Var,
    /// "while"
    While,

    // Other.
    /// Unexpected
    Unexpected { cause: String },
}
